//
//  CreateBookingMemberInfoPickerPresenter.swift
//  RoomBooking
//
//  Created by VI_Business on 08/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import PhoneNumberKit

/**
 *  Adds or updates information about booking event member
 **/
class CreateBookingMemberPickerPresenter {
    enum TextFieldTag: Int {
        case name
        case phone
        case email
    }
    
    private let viewModel: CreateBookingViewModel
    private let phoneValidator = PhoneNumberKit()
    private var disposeBag: DisposeBag!
    private var editedMember: RoomBookingEventMember?
    
    init(viewModel: CreateBookingViewModel) {
        self.viewModel = viewModel
    }
    
    func presentAddMemberPicker() {
        presentPicker(withMember: nil)
    }
    
    func presentEditMemberPicker(member: RoomBookingEventMember) {
        presentPicker(withMember: member)
    }
    
    private func presentPicker(withMember member: RoomBookingEventMember?) {
        editedMember = member ?? RoomBookingEventMember()
        disposeBag = DisposeBag()
        
        let title = member != nil ? "EditRoomBookingMember".localized : "AddRoomBookingMember".localized
        let controller = UIAlertController.init(title: title, message: nil, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK".localized, style: .default) { [weak self, weak controller] _ in
            let strongSelf = self!
            let strongController = controller!
            
            let editedMember = strongSelf.editedMember!
            let texts = strongController.textFields!.map {$0.text!}
            editedMember.name = texts[TextFieldTag.name.rawValue]
            editedMember.email = texts[TextFieldTag.email.rawValue]
            
            let phoneNumber = try! strongSelf.phoneValidator.parse(texts[TextFieldTag.phone.rawValue])
            editedMember.phone = strongSelf.phoneValidator.format(phoneNumber, toType: .e164)
            
            strongSelf.updateEventMembers()
        }
        
        controller.addTextField { (textField) in
            textField.tag = TextFieldTag.name.rawValue
            textField.text = member?.name
            textField.placeholder = "FullNamePlaceholder".localized
        }
        
        controller.addTextField { (textField) in
            textField.tag = TextFieldTag.phone.rawValue
            textField.keyboardType = .decimalPad
            textField.text = member?.phone
            textField.placeholder = "PhonePlaceholder".localized
        }
        
        controller.addTextField { (textField) in
            textField.tag = TextFieldTag.email.rawValue
            textField.keyboardType = .emailAddress
            textField.text = member?.email
            textField.placeholder = "EmailPlaceholder".localized
        }
        
        controller.addAction(okAction)
        controller.addAction(UIAlertAction(title: "Cancel".localized, style: .default))
        
        setupObservations(controller: controller, okAction: okAction)
        UIViewController.topmostViewController.present(controller, animated: true, completion: nil)
    }
    
    private func setupObservations(controller: UIAlertController, okAction: UIAlertAction) {
        let textFields = controller.textFields!
        Observable.combineLatest(textFields[TextFieldTag.name.rawValue].rx.text,
                                 textFields[TextFieldTag.email.rawValue].rx.text,
                                 textFields[TextFieldTag.phone.rawValue].rx.text)
            .map { [weak self] (name, email, phone) in
                return self!.validateFields(name: name, phone: phone, email: email)
            }.bind(to: okAction.rx.isEnabled).disposed(by: disposeBag)
    }
    
    private func validateFields(name: String?, phone: String?, email: String?) -> Bool {
        if name == nil || name!.isEmpty {
            return false
        }
        
        if phone == nil || (try? phoneValidator.parse(phone!)) == nil {
            return false
        }
    
        if email == nil || !email!.isValidEmail() {
            return false
        }
        
        return true
    }
    
    private func updateEventMembers() {
        var members = try! viewModel.eventMembers.value()
        let editedMemberIndex = members.firstIndex {$0 === editedMember!}
        if editedMemberIndex != nil {
            // Signal that existing member was updated
            viewModel.eventMembers.onNext(members)
        } else {
            members.append(editedMember!)
            viewModel.eventMembers.onNext(members)
        }
    }
}
