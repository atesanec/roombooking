//
//  CreateBookingTimePickerSectionManager.swift
//  RoomBooking
//
//  Created by VI_Business on 07/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

class CreateBookingTimePickerSectionManager: CollectionViewSectionManager {
    var collectionView: UICollectionView!
    var sectionIndex: Int = CreateBookingViewController.SectionIndex.timePicker.rawValue
    var dataObservable = BehaviorSubject<[Any]>(value: [])
    
    private let viewModel: CreateBookingViewModel
    private let disposeBag = DisposeBag()
    
    init(viewModel: CreateBookingViewModel) {
        self.viewModel = viewModel
        
        Observable<[Any]>.just([()]).bind(to: dataObservable).disposed(by: disposeBag)
        
        // Relax the intensity of heavy computations with debounce
        viewModel.bookingTime.debounce(0.1, scheduler: MainScheduler.instance).subscribe { [weak self] _ in
            guard let strongSelf = self, let collectionView = strongSelf.collectionView else {
                return
            }
            
            guard let cell = collectionView.cellForItem(at: IndexPath(row: 0, section: strongSelf.sectionIndex)) else {
                return
            }
            
            // Update time picker color
            self?.configure(cell: cell, atIndex: 0)
        }.disposed(by: disposeBag)
    }
    
    func registerCells() {
        collectionView.register(CreateBookingTimePickerCell.self, forCellWithReuseIdentifier: CreateBookingTimePickerCell.defaultReuseId)
    }
    
    func configure(cell: UICollectionViewCell, atIndex: Int) {
        let timeFrameCell = cell as! CreateBookingTimePickerCell
        let config = CreateBookingTimePickerCell.Configuration(selectedTimeInterval: try! viewModel.bookingTime.value(),
                                                               roomItem: viewModel.roomItem,
                                                               intervalSelectedObserver: viewModel.bookingTime.asObserver())
        
        timeFrameCell.configure(info: config)
    }
    
    func cellId(atIndex: Int) -> String {
        return CreateBookingTimePickerCell.defaultReuseId
    }
    
    func sizeForItem(atIndex: Int) -> CGSize {
        let width = collectionView.bounds.width
        return CGSize(width: width, height: CreateBookingTimePickerCell.cellHeight)
    }
    
    func itemSelected(atIndex: Int) {
    }
}
