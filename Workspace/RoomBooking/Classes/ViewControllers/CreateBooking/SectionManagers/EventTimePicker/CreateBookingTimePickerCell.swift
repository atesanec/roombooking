//
//  CreateBookingTimePickerCell.swift
//  RoomBooking
//
//  Created by VI_Business on 07/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

class CreateBookingTimePickerCell: UICollectionViewCell {
    struct Configuration {
        let selectedTimeInterval: CreateBookingTimeInterval?
        let roomItem: RoomForDayEntity
        let intervalSelectedObserver: AnyObserver<CreateBookingTimeInterval?>
    }
    
    static let cellHeight: CGFloat = 120
    private static let cellInset: CGFloat = 20
    
    private let timeFrameIndicator = TimeFrameIndicatorView()
    private let timeFramePicker = TimeFramePickerView()
    private let separator = OnePixelSeparatorView()
    private var timeFrameViewConfigurationAdapter: TimeFrameIndicatorConfigurationAdapter!
    private var timeFramePickerConfigurationAdapter: TimeFramePickerConfigurationAdapter!
    private var disposeBag = DisposeBag()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        contentView.addSubview(timeFrameIndicator)
        contentView.addSubview(timeFramePicker)
        contentView.addSubview(separator)
        
        timeFrameIndicator.style = .full
        timeFrameIndicator.translatesAutoresizingMaskIntoConstraints = false
        timeFrameIndicator.backgroundColor = .white
        
        timeFrameViewConfigurationAdapter = TimeFrameIndicatorConfigurationAdapter(timeFrameView: timeFrameIndicator)
        timeFramePickerConfigurationAdapter = TimeFramePickerConfigurationAdapter(timeFrameView: timeFramePicker)
    }
    
    func configure(info: Configuration) {
        timeFrameViewConfigurationAdapter.configure(withRoomItem: info.roomItem)
        timeFramePickerConfigurationAdapter.configure(withRoomItem: info.roomItem)
        
        timeFramePicker.selectedTimeInterval = (from: info.selectedTimeInterval!.fromTimeOffset, to: info.selectedTimeInterval!.toTimeOffset)
        timeFramePicker.timeIntervalSelectedSignal.map { (fromTimeOffset: $0.from, toTimeOffset: $0.to) }.bind(to: info.intervalSelectedObserver)
            .disposed(by: disposeBag)
    }
    
    override func prepareForReuse() {
        disposeBag = DisposeBag()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let availableHeight = bounds.height - CreateBookingTimePickerCell.cellInset
        
        let indicatorFrame = CGRect(origin: .zero, size: CGSize(width: bounds.width, height: availableHeight * 0.6))
        timeFrameIndicator.frame = indicatorFrame
        
        let timeBarFrame = timeFrameIndicator.timeScaleBarFrame()
        let pickerFrame = CGRect(origin: timeBarFrame.origin, size: CGSize(width: timeBarFrame.width,
                                                                           height: availableHeight - timeBarFrame.origin.y))
        timeFramePicker.frame = pickerFrame
        timeFramePicker.intervalBarHeight = timeFrameIndicator.timeScaleBarFrame().height
        
        separator.placeIntoView(position: .bottom)
    }
}
