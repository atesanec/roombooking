//
//  CreateBookingInfoEditSectionManager.swift
//  RoomBooking
//
//  Created by VI_Business on 07/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

class CreateBookingInfoEditSectionManager: CollectionViewSectionManager {
    enum TextFieldIndex: Int {
        case eventTitle
        case eventDescription
    }
    
    var collectionView: UICollectionView!
    var sectionIndex: Int = CreateBookingViewController.SectionIndex.infoEdit.rawValue
    var dataObservable = BehaviorSubject<[Any]>(value: [])
    
    private static let textFieldFont = UIFont.systemFont(ofSize: 18)
    
    private let viewModel: CreateBookingViewModel
    private let disposeBag = DisposeBag()
    private let cellHeightChangeObserver = PublishSubject<CGFloat>()
    
    init(viewModel: CreateBookingViewModel) {
        self.viewModel = viewModel
        Observable<[Any]>.just([TextFieldIndex.eventTitle, TextFieldIndex.eventDescription]).bind(to: dataObservable).disposed(by: disposeBag)
        
        cellHeightChangeObserver.subscribe { [weak self] _ in
            let collectionView = self?.collectionView
            collectionView?.collectionViewLayout.invalidateLayout()
            collectionView?.setNeedsLayout()
            collectionView?.layoutIfNeeded()
        }.disposed(by: disposeBag)
    }
    
    func registerCells() {
        collectionView.register(MultilineTextFieldCell.self, forCellWithReuseIdentifier: MultilineTextFieldCell.defaultReuseId)
    }
    
    func configure(cell: UICollectionViewCell, atIndex: Int) {
        let textCell = cell as! MultilineTextFieldCell
        let textFieldIndex = try! self.dataObservable.value()[atIndex] as! TextFieldIndex
        
        var placeholder: String? = nil
        var text: String? = nil
        let textChangeSignal = PublishSubject<String?>()
        let trimmedTextObservable = textChangeSignal.map {$0?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)}
        switch textFieldIndex {
        case .eventTitle:
            placeholder = "BookingEventTitle".localized
            text = try! viewModel.eventTitle.value()
            trimmedTextObservable.bind(to: viewModel.eventTitle).disposed(by: disposeBag)
            
        case .eventDescription:
            placeholder = "BookingEventDescription".localized
            text = try! viewModel.eventDescription.value()
            trimmedTextObservable.bind(to: viewModel.eventDescription).disposed(by: disposeBag)
        }

        textCell.configure(info: MultilineTextFieldCell.Configuration(textFont: CreateBookingInfoEditSectionManager.textFieldFont,
                                                                      placeholder: placeholder!,
                                                                      text: text,
                                                                      textChangedObserver: textChangeSignal.asObserver(),
                                                                      heightChangedObserver: cellHeightChangeObserver.asObserver()))
    }
    
    func cellId(atIndex: Int) -> String {
        return MultilineTextFieldCell.defaultReuseId
    }
    
    func sizeForItem(atIndex: Int) -> CGSize {
        let width = collectionView.bounds.width
        let textFieldIndex = try! self.dataObservable.value()[atIndex] as! TextFieldIndex
        
        var text: String? = nil
        switch textFieldIndex {
        case .eventTitle:
            text = try! viewModel.eventTitle.value()
            
        case .eventDescription:
            text = try! viewModel.eventDescription.value()
        }
        
        var height = MultilineTextFieldCell.minCellHeight
        if let inputText = text {
            let measuredHeight = inputText.heightWithConstrainedWidth(width: width, font: CreateBookingInfoEditSectionManager.textFieldFont)
            height = max(height, measuredHeight + 2 * MultilineTextFieldCell.textVerticalInset)
        }
        
        return CGSize(width: width, height: height)
    }
    
    func itemSelected(atIndex: Int) {
    }
}
