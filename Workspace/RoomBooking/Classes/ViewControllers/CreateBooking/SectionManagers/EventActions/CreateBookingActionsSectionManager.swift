//
//  CreateBookingActionsSectionManager.swift
//  RoomBooking
//
//  Created by VI_Business on 07/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

class CreateBookingActionsSectionManager: CollectionViewSectionManager {
    var collectionView: UICollectionView!
    var sectionIndex: Int = CreateBookingViewController.SectionIndex.actions.rawValue
    var dataObservable = BehaviorSubject<[Any]>(value: [])
    
    private let viewModel: CreateBookingViewModel
    private let disposeBag = DisposeBag()
    private let memberPicker: CreateBookingMemberPickerPresenter
    
    init(viewModel: CreateBookingViewModel) {
        self.viewModel = viewModel
        memberPicker = CreateBookingMemberPickerPresenter(viewModel: viewModel)
        
        Observable<[Any]>.just([()]).bind(to: dataObservable).disposed(by: disposeBag)
    }
    
    func registerCells() {
        collectionView.register(CreateBookingAddMemberActionCell.self, forCellWithReuseIdentifier: CreateBookingAddMemberActionCell.defaultReuseId)
    }
    
    func configure(cell: UICollectionViewCell, atIndex: Int) {
    }
    
    func cellId(atIndex: Int) -> String {
        return CreateBookingAddMemberActionCell.defaultReuseId
    }
    
    func sizeForItem(atIndex: Int) -> CGSize {
        let width = collectionView.bounds.width
        return CGSize(width: width, height: CreateBookingAddMemberActionCell.cellHeight)
    }
    
    func itemSelected(atIndex: Int) {
        memberPicker.presentAddMemberPicker()
    }
}
