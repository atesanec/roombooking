//
//  CreateBookingAddMemberActionCell.swift
//  RoomBooking
//
//  Created by VI_Business on 07/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

class CreateBookingAddMemberActionCell: UICollectionViewCell {
    static let cellHeight: CGFloat = 60
    
    private let textLabel = UILabel()
    private let separator = OnePixelSeparatorView()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        contentView.addSubview(textLabel)
        contentView.addSubview(separator)
        
        textLabel.textAlignment = .center
        textLabel.text = "AddEventMember".localized
        textLabel.textColor = .celurean
    }
    
    override func layoutSubviews() {
        textLabel.frame = self.bounds
        separator.placeIntoView(position: .bottom)
    }
}
