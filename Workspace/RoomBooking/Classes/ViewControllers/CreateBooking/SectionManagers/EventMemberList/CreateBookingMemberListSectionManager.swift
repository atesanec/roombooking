//
//  CreateBookingMemberListSectionManager.swift
//  RoomBooking
//
//  Created by VI_Business on 08/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

class CreateBookingMemberListSectionManager: CollectionViewSectionManager {
    var collectionView: UICollectionView!
    var sectionIndex: Int = CreateBookingViewController.SectionIndex.memberList.rawValue
    var dataObservable = BehaviorSubject<[Any]>(value: [])
    
    private let viewModel: CreateBookingViewModel
    private let disposeBag = DisposeBag()
    private let memberMenu: CreateBookingMemberMenuManager
    
    init(viewModel: CreateBookingViewModel) {
        self.viewModel = viewModel
        memberMenu = CreateBookingMemberMenuManager(viewModel: viewModel)
        
        self.viewModel.eventMembers.flatMap({ (items) in
            return Observable<[Any]>.just(items)
        }).bind(to: dataObservable).disposed(by: disposeBag)
    }
    
    func registerCells() {
        collectionView.register(CreateBookingMemberListCell.self, forCellWithReuseIdentifier: CreateBookingMemberListCell.defaultReuseId)
    }
    
    func configure(cell: UICollectionViewCell, atIndex: Int) {
        let item = try! self.dataObservable.value()[atIndex] as! RoomBookingEventMember
        
        let config = CreateBookingMemberListCell.Configuration(member: item)
        let itemCell = cell as! CreateBookingMemberListCell
        itemCell.configure(info: config)
    }
    
    func cellId(atIndex: Int) -> String {
        return CreateBookingMemberListCell.defaultReuseId
    }
    
    func sizeForItem(atIndex: Int) -> CGSize {
        let width = collectionView.bounds.width
        return CGSize(width: width, height: CreateBookingMemberListCell.cellHeight)
    }
    
    func itemSelected(atIndex: Int) {
        let item = try! self.dataObservable.value()[atIndex] as! RoomBookingEventMember
        memberMenu.presentMenu(member: item)
    }
}
