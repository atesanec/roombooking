//
//  CreateBookingMemberListCell.swift
//  RoomBooking
//
//  Created by VI_Business on 08/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

class CreateBookingMemberListCell: UICollectionViewCell {
    struct Configuration {
        let member: RoomBookingEventMember
    }
    
    static let cellHeight: CGFloat = 100
    
    private let textLabel = UILabel()
    private let separator = OnePixelSeparatorView()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        contentView.addSubview(textLabel)
        contentView.addSubview(separator)
        
        textLabel.font = UIFont.systemFont(ofSize: 16)
        textLabel.numberOfLines = 0
    }
    
    func configure(info: Configuration) {
        let member = info.member
        var memberInfo = String(format:"EventMemberNameInfo".localized + "\r\n", member.name!)
        memberInfo += String(format:"EventMemberEmailInfo".localized + "\r\n", member.email!)
        memberInfo += String(format:"EventMemberPhoneInfo".localized, member.phone!)
        textLabel.text = memberInfo
        
        separator.placeIntoView(position: .bottom)
    }
    
    override func layoutSubviews() {
        textLabel.frame = self.bounds
    }
}
