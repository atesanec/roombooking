//
//  CreateBookingActionManager.swift
//  RoomBooking
//
//  Created by VI_Business on 08/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

class CreateBookingActionManager {
    let viewModel: CreateBookingViewModel
    private let disposeBag = DisposeBag()
    
    init(viewModel: CreateBookingViewModel) {
        self.viewModel = viewModel
    }
    
    func createBooking() {
        let analytics = RoomForDayTimeAnalyticsAdapter(roomEntity: viewModel.roomItem)
        let interval = try! viewModel.bookingTime.value()!
        let event = RoomBookingEvent(title: try! viewModel.eventTitle.value()!,
                                     description: try! viewModel.eventDescription.value()!,
                                     room: viewModel.roomItem,
                                     timeSlots: analytics.vacantSlotsForWholeInterval(from: interval.fromTimeOffset, to: interval.toTimeOffset)!,
                                     members: try! viewModel.eventMembers.value())
        RootServicesDomain.sharedInstance.roomBookingNetworkService.createBookingEvent(event: event)
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { (success) in
                ViewController.topmostViewController.dismiss(animated: true) {
                    let alertPresenter = RootServicesDomain.sharedInstance.alertMessagePresenter
                    alertPresenter.presentAlert(title: "BookingCreatedSuccess".localized,
                                                text: nil)
                }
            }, onError: { (error) in
                let errorText = String.init(format: "BookingCreatedFailed".localized, error.localizedDescription)
                RootServicesDomain.sharedInstance.alertMessagePresenter.presentAlert(title: errorText, text: nil)
            }).disposed(by: disposeBag)
    }
}
