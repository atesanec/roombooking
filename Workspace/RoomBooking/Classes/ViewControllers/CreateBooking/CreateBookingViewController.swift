//
//  CreateBookingViewController.swift
//  RoomBooking
//
//  Created by VI_Business on 07/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

/**
 *  Creating a room boking event
 */
class CreateBookingViewController: UIViewController {
    enum SectionIndex: Int {
        case timePicker
        case infoEdit
        case actions
        case memberList
    }
    
    private let viewModel: CreateBookingViewModel
    private var sectionManagerAggregate: CollectionViewSectionManagerAggregate!
    private let disposeBag = DisposeBag()
    private let actionManager: CreateBookingActionManager
    
    private static let collectionViewTopInset: CGFloat = 20
    
    @IBOutlet private weak var collectionView: UICollectionView!

    init(roomItem: RoomForDayEntity) {
        self.viewModel = CreateBookingViewModel(roomItem: roomItem)
        self.actionManager = CreateBookingActionManager(viewModel: viewModel)
        super.init(nibName: "CreateBookingViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        assert(false)
        return nil
    }

    func presentFromTompostController() {
        let controller = UINavigationController(rootViewController: self)
        UIViewController.topmostViewController.present(controller, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        let timePickerSection = CreateBookingTimePickerSectionManager(viewModel: viewModel)
        let infoEditSection = CreateBookingInfoEditSectionManager(viewModel: viewModel)
        let actionsSection = CreateBookingActionsSectionManager(viewModel: viewModel)
        let membersList = CreateBookingMemberListSectionManager(viewModel: viewModel)
        sectionManagerAggregate = CollectionViewSectionManagerAggregate(sectionManagers: [timePickerSection,
                                                                                          infoEditSection,
                                                                                          actionsSection,
                                                                                          membersList],
                                                                        collectionView: collectionView)
        
        configureUI()
        setupUIObservations()
        setupModelObservations()
    }
    
    private func configureUI() {
        self.title = "CreateEvent".localized
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .cancel, target: nil, action: nil)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .done, target: nil, action: nil)
        
        var inset = collectionView.contentInset
        inset.top = CreateBookingViewController.collectionViewTopInset
        collectionView.contentInset = inset
        collectionView.scrollIndicatorInsets = inset
    }
    
    private func setupModelObservations() {
        viewModel.isBookingPossible.bind(to: self.navigationItem.rightBarButtonItem!.rx.isEnabled).disposed(by: disposeBag)
    }
    
    private func setupUIObservations() {
        self.navigationItem.leftBarButtonItem!.rx.tap.subscribe { [weak self] _ in
            self?.dismiss(animated: true, completion: nil)
            }.disposed(by: disposeBag)
        
        self.navigationItem.rightBarButtonItem!.rx.tap.subscribe { [weak self] _ in
            self?.actionManager.createBooking()
            }.disposed(by: disposeBag)
    }
}
