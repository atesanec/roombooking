//
//  CreateBookingViewModel.swift
//  RoomBooking
//
//  Created by VI_Business on 07/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

typealias CreateBookingTimeInterval = (fromTimeOffset: TimeInterval, toTimeOffset: TimeInterval)

/**
 *  View model for creating a room boking event
 */
class CreateBookingViewModel: NSObject {
    let bookingTime = BehaviorSubject<CreateBookingTimeInterval?>(value: nil)
    let eventTitle = BehaviorSubject<String?>(value: nil)
    let eventDescription = BehaviorSubject<String?>(value: nil)
    let eventMembers = BehaviorSubject<[RoomBookingEventMember]>(value: [])
    let isBookingPossible = BehaviorSubject<Bool>(value: false)
    let roomItem: RoomForDayEntity
    
    private let disposeBag = DisposeBag()
    
    init(roomItem: RoomForDayEntity) {
        self.roomItem = roomItem
        
        let analytics = RoomForDayTimeAnalyticsAdapter(roomEntity: roomItem)
        let availableInterval = analytics.totalSlotTimeInterval
        let reducedIntervalDuration = (availableInterval.to - availableInterval.from) * 0.25
        bookingTime.onNext((fromTimeOffset: availableInterval.from + reducedIntervalDuration,
                            toTimeOffset: availableInterval.to - reducedIntervalDuration))
        
        Observable.combineLatest(bookingTime,
                                 eventTitle,
                                 eventDescription,
                                 eventMembers).map { (bookingTime, title, description, members) in
                                    guard let bookTime = bookingTime else {
                                        return false
                                    }
                                    
                                    if analytics.vacantSlotsForWholeInterval(from: bookTime.fromTimeOffset, to: bookTime.toTimeOffset) == nil {
                                        return false
                                    }
                                    
                                    if title == nil || title!.isEmpty {
                                        return false
                                    }
                                    
                                    if description == nil || description!.isEmpty {
                                        return false
                                    }
                                    
                                    if members.isEmpty {
                                        return false
                                    }
                                    
                                    return true
        }.bind(to: isBookingPossible).disposed(by: disposeBag)
    }
}
