//
//  CreateBookingMemberMenuManager.swift
//  RoomBooking
//
//  Created by VI_Business on 08/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

/**
 *  Presents menu for event mamber item
 */
class CreateBookingMemberMenuManager {
    private let viewModel: CreateBookingViewModel
    private let memberPicker: CreateBookingMemberPickerPresenter

    init(viewModel: CreateBookingViewModel) {
        self.viewModel = viewModel
        memberPicker = CreateBookingMemberPickerPresenter(viewModel: viewModel)
    }
    
    func presentMenu(member: RoomBookingEventMember) {
        let title = member.name
        let controller = UIAlertController.init(title: title, message: nil, preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Delete".localized, style: .destructive) { [weak self] _ in
            guard let strongSelf = self else {
                return
            }
            
            var members = try! strongSelf.viewModel.eventMembers.value()
            let memberIndex = members.firstIndex {$0 === member}!
            members.remove(at: memberIndex)
            strongSelf.viewModel.eventMembers.onNext(members)
        }
        
        let editAction = UIAlertAction(title: "Edit".localized, style: .default) { [weak self] _ in
            guard let strongSelf = self else {
                return
            }
    
            strongSelf.memberPicker.presentEditMemberPicker(member: member)
        }
        
        controller.addAction(editAction)
        controller.addAction(deleteAction)
        controller.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil))
        UIViewController.topmostViewController.present(controller, animated: true, completion: nil)
    }
}
