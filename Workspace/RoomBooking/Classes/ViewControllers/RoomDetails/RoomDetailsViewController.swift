//
//  RoomDetailsViewController.swift
//  RoomBooking
//
//  Created by VI_Business on 06/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

/**
 *  Detailed information about the room
 */
class RoomDetailsViewController: UIViewController {
    @IBOutlet private weak var roomTimeFrameView: TimeFrameIndicatorView!
    @IBOutlet private weak var roomInfoLabel: UILabel!
    @IBOutlet private weak var imagesStackView: UIStackView!
    @IBOutlet private weak var bookButton: UIButton!
    
    private let viewModel: RoomDetailsViewModel
    private let disposeBag = DisposeBag()
    private let timeSlotsMonitor: RoomTimeSlotsMonitor
    
    init(roomItem: RoomForDayEntity) {
        viewModel = RoomDetailsViewModel(roomItem: roomItem)
        timeSlotsMonitor = RoomTimeSlotsMonitor(room: roomItem)
        super.init(nibName: "RoomDetailsViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        return nil
    }
    
    override func viewDidLoad() {
        self.title = "RoomInfo".localized
        
        bookButton.setTitle("BookNow".localized, for: .normal)
        bookButton.rx.tap.subscribe { [weak self] _ in
            guard let strongSelf = self else {
                return
            }
            
            let controller = CreateBookingViewController(roomItem: strongSelf.viewModel.roomItem)
            controller.presentFromTompostController()
        }.disposed(by: disposeBag)
        
        roomInfoLabel.attributedText = createRoomDescriptionText()
        populateRoomImages()
        populateTimeFrame()
        setupObservations()
    }
    
    private func createRoomDescriptionText() -> NSAttributedString {
        let room = viewModel.roomItem
        var fragmentLocation = 0
        let text = NSMutableAttributedString(string: viewModel.roomItem.name! + "\r\n\r\n")
        text.setAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20)],
                           range: NSRange(location: fragmentLocation, length: text.length))
        
        fragmentLocation += text.length
        var roomDetails = String(format: "RoomLocation".localized + "\r\n", room.location!)
        roomDetails += String(format: "RoomCapacity".localized + "\r\n", room.roomCapacity)
        roomDetails += String(format: "RoomSize".localized + "\r\n\r\n", room.sizeSquareMeters)
        
        roomDetails += "Equipment".localized + "\r\n"
        roomDetails += room.equipment!.map {"- " + $0}.joined(separator: "\r\n")
        text.mutableString.append(roomDetails)
        text.setAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)],
                           range: NSRange(location: fragmentLocation, length: (roomDetails as NSString).length))
        
        return text
    }
    
    private func populateRoomImages() {
        for imageURL in viewModel.roomItem.images! {
            let imageView = UIImageView()
            imageView.clipsToBounds = true
            imageView.contentMode = .scaleAspectFill
            imagesStackView.addArrangedSubview(imageView)
            imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: 1.0).isActive = true
            imageView.widthAnchor.constraint(equalTo: imagesStackView.widthAnchor, multiplier: 1.0).isActive = true
            
            RootServicesDomain.sharedInstance.imageDownloader.downloadImage(url: imageURL).bind(to: imageView.rx.image)
                .disposed(by: disposeBag)
        }
        
        let fillerView = imagesStackView.arrangedSubviews.first!
        imagesStackView.addArrangedSubview(fillerView)
    }
    
    private func populateTimeFrame() {
        roomTimeFrameView.style = .full
        
        let adapter = TimeFrameIndicatorConfigurationAdapter(timeFrameView: roomTimeFrameView)
        adapter.configure(withRoomItem: viewModel.roomItem)
    }
    
    func setupObservations() {
        timeSlotsMonitor.startMonitoring()
        timeSlotsMonitor.timeSlotsUpdatedSignal.subscribe(onNext: { [weak self] ([RoomForDayTimeSlotEntity]) in
            self?.populateTimeFrame()
        }).disposed(by: disposeBag)
    }
}
