//
//  RoomDetailsViewModel.swift
//  RoomBooking
//
//  Created by VI_Business on 07/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

/**
 *  View model for the detailed information about the room
 */
class RoomDetailsViewModel {
    let roomItem: RoomForDayEntity
    
    init(roomItem: RoomForDayEntity) {
        self.roomItem = roomItem
    }
}
