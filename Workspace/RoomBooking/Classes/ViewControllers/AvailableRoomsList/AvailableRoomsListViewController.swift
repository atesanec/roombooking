//
//  AvailableRoomsListViewController.swift
//  RoomBooking
//
//  Created by VI_Business on 04/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

/**
 *  List of rooms available for rent
 */
class AvailableRoomsListViewController: UIViewController {
    enum SectionIndex: Int {
        case listItems
    }
    
    private static let titleDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        return dateFormatter
    }()
    
    private var sectionManagerAggregate: CollectionViewSectionManagerAggregate!
    private let viewModel = AvailableRoomsListViewModel()
    private var listLoader: AvailableRoomsListLoader!
    private var listPlaceholderController: LoadedListPlaceholderViewController!
    private let disposeBag = DisposeBag()
    private var roomCapacityPicker: AvailableRoomsListRoomCapacityPicker!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var availableNextHourSwitch: UISwitch!
    @IBOutlet weak var availableNextHourLabel: UILabel!
    @IBOutlet weak var capacityFilterButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!

    override func viewDidLoad() {
        let listSectionManager = AvailableRoomsListItemSectionManager(viewModel: viewModel)
        sectionManagerAggregate = CollectionViewSectionManagerAggregate(sectionManagers: [listSectionManager], collectionView: collectionView)
        
        listLoader = AvailableRoomsListLoader(viewModel: viewModel)
        listLoader.filteredRoomItems.bind(to: viewModel.loadedRoomItems).disposed(by: disposeBag)
        
        configureUI()
        setupUIObservations()
        setupModelObservations()
    }
    
    private func configureUI() {

        searchBar.placeholder = "AvailableRoomSearchPlaceholder".localized
        availableNextHourLabel.text = "AvailableNextHour".localized
        
        listPlaceholderController = LoadedListPlaceholderViewController()
        self.embed(childViewController: listPlaceholderController)
        listPlaceholderController.view.frame = collectionView.frame
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Calendar".localized, style: .plain, target: self,
                                                                action: #selector(onCalendarTap(sender:)))
        
        roomCapacityPicker = AvailableRoomsListRoomCapacityPicker(viewModel: viewModel)
    }
    
    override func viewWillLayoutSubviews() {
        listPlaceholderController.view.frame = collectionView.frame
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Time slots may be updated
        sectionManagerAggregate.reconfigureVisibleCells()
    }
    
    private func setupModelObservations() {
        viewModel.loadedRoomItems.map({$0.count}).bind(to: listPlaceholderController.itemCountObserver).disposed(by: disposeBag)
        viewModel.isLoading.bind(to: listPlaceholderController.isLoadingObserver).disposed(by: disposeBag)
        
        viewModel.selectedDate.subscribe(onNext: { [weak self] (date) in
            // Rooms are shown for the room service time zone, so the day
            self?.title = AvailableRoomsListViewController.titleDateFormatter.string(from: date.localTimeToRoomBookingServiceTime())
        }).disposed(by: disposeBag)
        
        viewModel.roomCapacityFilter.subscribe(onNext: { [weak self] (filterValue) in
            guard let strongSelf = self else {
                return
            }
            
            var filterValueForTitle: String? = nil
            if let value = filterValue {
                filterValueForTitle = String(value)
            } else {
                filterValueForTitle = "AnyRoomCapacity".localized
            }
            
            let title = String(format:"MinRoomCapacity".localized, filterValueForTitle!)
            strongSelf.capacityFilterButton.setTitle(title, for: .normal)
        }).disposed(by: disposeBag)
    }
    
    private func setupUIObservations() {
        searchBar.rx.text.ifEmpty(default: "")
            .map({$0!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)})
            .distinctUntilChanged()
            .bind(to: viewModel.roomNameFilter)
            .disposed(by: disposeBag)
        
        listPlaceholderController.reloadButtonTappedObservable.subscribe(onNext: { [weak self] _ in
            self?.listLoader.reloadList()
        }).disposed(by: disposeBag)
        
        availableNextHourSwitch.rx.value.bind(to: viewModel.shouldShowAvailableNextHourOnly).disposed(by: disposeBag)
        capacityFilterButton.rx.tap.subscribe(onNext: { [weak self] _ in
            self?.roomCapacityPicker.presentPicker()
        }).disposed(by: disposeBag)
    }
    
    @objc private func onCalendarTap(sender: Any) {
        let calendar = CalendarDatePickerViewController()
        calendar.dateSelectedSignal.subscribe(onNext: { [weak self, weak calendar] (date) in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.viewModel.selectedDate.onNext(date)
            calendar?.dismiss(animated: true, completion: nil)
        }).disposed(by: disposeBag)
        
        calendar.pickerCancelledSignal.subscribe(onNext: { [weak calendar] (date) in
            calendar?.dismiss(animated: true, completion: nil)
        }).disposed(by: disposeBag)
        
        calendar.presentFromTopmostController()
    }
}
