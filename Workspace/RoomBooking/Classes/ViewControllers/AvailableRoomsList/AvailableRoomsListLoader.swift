//
//  AvailableRoomsListLoader.swift
//  RoomBooking
//
//  Created by VI_Business on 04/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

/**
 *  Handles room list reload and filtering logic
 */
class AvailableRoomsListLoader {
    private let viewModel: AvailableRoomsListViewModel
    private let loadedRoomItems = BehaviorSubject<[RoomForDayEntity]>(value: [])
    private let disposeBag = DisposeBag()
    
    let filteredRoomItems = BehaviorSubject<[RoomForDayEntity]>(value: [])
    
    init(viewModel: AvailableRoomsListViewModel) {
        self.viewModel = viewModel

        setupModelObservations()
    }
    
    func reloadList() -> Void {
        let isLoading = try! self.viewModel.isLoading.value()
        if !isLoading {
            self.viewModel.isLoading.onNext(true)
            RootServicesDomain.sharedInstance.roomBookingNetworkService
                .loadAvailableRooms(forDate: try! viewModel.selectedDate.value())
                .subscribeOn(MainScheduler.instance)
                .subscribe(
                    onNext: { [weak self] (entities) in
                        guard let strongSelf = self else {
                            return
                        }
                        
                        strongSelf.viewModel.isLoading.onNext(false)
                        strongSelf.loadedRoomItems.onNext(entities)
                    },
                    onError: { [weak self] (error) in
                        guard let strongSelf = self else {
                            return
                        }
                        
                        strongSelf.viewModel.isLoading.onNext(false)
                        RootServicesDomain.sharedInstance.alertMessagePresenter.presentAlert(title: "CannotLoadRooms".localized,
                                                                                             text: error.localizedDescription)
                }).disposed(by: disposeBag)
        }
    }
    
    private func setupModelObservations() {
        viewModel.selectedDate.subscribe(onNext: { [weak self] (date) in
            self?.reloadList()
        }).disposed(by: disposeBag)
        
        Observable.combineLatest(
            loadedRoomItems.asObservable(),
            viewModel.roomCapacityFilter.asObservable(),
            viewModel.roomNameFilter.asObservable(),
            viewModel.shouldShowAvailableNextHourOnly.asObservable()
            ).map { [weak self] (list, capacityFilter, nameFilter, nextHourFilter) in
                let strongSelf = self!
                return strongSelf.filterRoomItems(list: list, capacityFilter: capacityFilter, nameFilter: nameFilter,
                                                  showAvailableNextHourOnly: nextHourFilter)
            }.bind(to: self.filteredRoomItems).disposed(by: disposeBag)
    }
    
    private func filterRoomItems(list: [RoomForDayEntity],
                                 capacityFilter: Int?,
                                 nameFilter: String?,
                                 showAvailableNextHourOnly: Bool) -> [RoomForDayEntity] {
        var resultList = list
        if let nameFilter = nameFilter {
            resultList = resultList.filter {$0.name!.starts(with: nameFilter)}
        }
        
        if showAvailableNextHourOnly {
            resultList = resultList.filter {RoomForDayTimeAnalyticsAdapter(roomEntity: $0).isAvailableNextHour}
        }
        
        if let minCapacity = capacityFilter {
            resultList = resultList.filter {$0.roomCapacity >= minCapacity}
        }
        
        return resultList
    }
}
