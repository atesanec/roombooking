//
//  AvailableRoomsListViewModel.swift
//  RoomBooking
//
//  Created by VI_Business on 04/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

/**
 *  View model for available rooms list
 */
class AvailableRoomsListViewModel {
    let selectedDate = BehaviorSubject<Date>(value: Date())
    let roomNameFilter = BehaviorSubject<String?>(value: nil)
    let shouldShowAvailableNextHourOnly = BehaviorSubject<Bool>(value: false)
    let roomCapacityFilter = BehaviorSubject<Int?>(value: nil)
    let loadedRoomItems = BehaviorSubject<[RoomForDayEntity]>(value: [RoomForDayEntity]())
    let isLoading = BehaviorSubject<Bool>(value: false)
}
