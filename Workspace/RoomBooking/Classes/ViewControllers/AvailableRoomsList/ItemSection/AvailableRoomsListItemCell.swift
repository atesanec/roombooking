//
//  AvailableRoomsListItemCell.swift
//  RoomBooking
//
//  Created by VI_Business on 04/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

class AvailableRoomsListItemCell: UICollectionViewCell {
    struct Configuration {
        let image: Observable<UIImage?>?
        let roomItem: RoomForDayEntity
    }
    
    static let cellHeight: CGFloat = 60
    private static let imagePadding: CGFloat = 4
    private static let timeIndicatorHeight: CGFloat = 10
    private static let disclosureIndicatorHeight: CGFloat = 16
    
    private let imageView = UIImageView()
    private let titleLabel = UILabel()
    private let descriptionLabel = UILabel()
    private let disclosureIndicator = UIImageView(image: UIImage.init(named: "cell_disclosure_indicator"))
    private let timeIndicatorView = TimeFrameIndicatorView()
    private let separatorView = OnePixelSeparatorView()
    private var disposeBag = DisposeBag()
    private var timeFrameViewConfigurationAdapter: TimeFrameIndicatorConfigurationAdapter!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        contentView.addSubview(imageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(disclosureIndicator)
        contentView.addSubview(timeIndicatorView)
        contentView.addSubview(separatorView)
        
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        timeIndicatorView.style = .minimal
        timeIndicatorView.contentMode = .redraw
        
        titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
        
        descriptionLabel.font = UIFont.systemFont(ofSize: 13, weight: .medium)
        descriptionLabel.textColor = .gray
        
        timeFrameViewConfigurationAdapter = TimeFrameIndicatorConfigurationAdapter(timeFrameView: timeIndicatorView)
    }
    
    func configure(info: Configuration) {
        imageView.image = nil
        info.image?.bind(to: imageView.rx.image).disposed(by: disposeBag)
        
        let item = info.roomItem
        titleLabel.text = item.name
        descriptionLabel.text = String(format: "RoomListItemDescription".localized, item.location!, item.roomCapacity)
        
        timeFrameViewConfigurationAdapter.configure(withRoomItem: info.roomItem)
    }
    
    override func prepareForReuse() {
        disposeBag = DisposeBag()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let imageHeight = contentView.bounds.height - AvailableRoomsListItemCell.imagePadding * 2
        var imageFrame = CGRect(origin: CGPoint.zero, size: CGSize(width: imageHeight, height: imageHeight))
        imageFrame.origin = CGPoint(x: AvailableRoomsListItemCell.imagePadding, y: AvailableRoomsListItemCell.imagePadding)
        imageView.frame = imageFrame
        
        var disclosureFrame = disclosureIndicator.frame
        disclosureFrame.size.height = AvailableRoomsListItemCell.disclosureIndicatorHeight
        disclosureFrame.size.width = disclosureFrame.size.height
        disclosureFrame.origin = CGPoint(x: bounds.maxX - disclosureFrame.width - AvailableRoomsListItemCell.imagePadding,
                                         y: bounds.midY - disclosureFrame.height * 0.5)
        disclosureIndicator.frame = disclosureFrame
        
        var timeIndicatorFrame = CGRect()
        timeIndicatorFrame.origin = CGPoint(x: imageFrame.maxX + AvailableRoomsListItemCell.imagePadding,
                                            y: AvailableRoomsListItemCell.imagePadding)
        timeIndicatorFrame.size = CGSize(width: disclosureFrame.minX - AvailableRoomsListItemCell.imagePadding - timeIndicatorFrame.minX,
                                          height: AvailableRoomsListItemCell.timeIndicatorHeight)
        timeIndicatorView.frame = timeIndicatorFrame
        
        var titleFrame = titleLabel.frame
        titleFrame.origin = CGPoint(x: timeIndicatorFrame.minX, y: timeIndicatorFrame.maxY + AvailableRoomsListItemCell.imagePadding)
        titleFrame.size = CGSize(width: timeIndicatorFrame.width,
                                 height: ceil(titleLabel.font.lineHeight))
        titleLabel.frame = titleFrame
        
        var descriptionFrame = descriptionLabel.frame
        descriptionFrame.size.width = titleFrame.width
        descriptionFrame.size.height = ceil(descriptionLabel.font.lineHeight)
        descriptionFrame.origin = CGPoint(x: titleFrame.minX, y: bounds.height - descriptionFrame.height - AvailableRoomsListItemCell.imagePadding)
        descriptionLabel.frame = descriptionFrame
        
        separatorView.placeIntoView(position: .bottom, offsetX: AvailableRoomsListItemCell.imagePadding)
    }
}
