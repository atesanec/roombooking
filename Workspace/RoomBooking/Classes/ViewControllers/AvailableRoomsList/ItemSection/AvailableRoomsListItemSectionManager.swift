//
//  AvailableRoomsListItemSectionManager.swift
//  RoomBooking
//
//  Created by VI_Business on 04/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AvailableRoomsListItemSectionManager: CollectionViewSectionManager {
    let viewModel: AvailableRoomsListViewModel
    let disposeBag = DisposeBag()
    var collectionView: UICollectionView!
    let sectionIndex: Int
    let dataObservable = BehaviorSubject<[Any]>(value: [])
    
    init(viewModel: AvailableRoomsListViewModel) {
        self.sectionIndex = AvailableRoomsListViewController.SectionIndex.listItems.rawValue
        self.viewModel = viewModel
        
        viewModel.loadedRoomItems.flatMap({ (items) in
            return Observable<[Any]>.just(items)
        }).bind(to: dataObservable).disposed(by: disposeBag)
    }
    
    func registerCells() {
        collectionView.register(AvailableRoomsListItemCell.self, forCellWithReuseIdentifier: AvailableRoomsListItemCell.defaultReuseId)
    }
    
    func configure(cell: UICollectionViewCell, atIndex: Int) {
        let item = try! self.dataObservable.value()[atIndex] as! RoomForDayEntity
        var imageObservable: Observable<UIImage?>? = nil
        if let firstImageURL = item.images?.first {
            let imageURL = firstImageURL
            imageObservable = RootServicesDomain.sharedInstance.imageDownloader.downloadImage(url: imageURL).asDriver(onErrorJustReturn: nil).asObservable()
        }
        
        let config = AvailableRoomsListItemCell.Configuration(image: imageObservable, roomItem: item)
        
        let itemCell = cell as! AvailableRoomsListItemCell
        itemCell.configure(info: config)
    }
    
    func cellId(atIndex: Int) -> String {
        return AvailableRoomsListItemCell.defaultReuseId
    }
    
    func sizeForItem(atIndex: Int) -> CGSize {
        let width = collectionView.bounds.width
        return CGSize(width: width, height: AvailableRoomsListItemCell.cellHeight)
    }
    
    func itemSelected(atIndex: Int) {
        let item = try! self.dataObservable.value()[atIndex] as! RoomForDayEntity
        let controller = RoomDetailsViewController(roomItem: item)
        ViewController.topmostViewController.navigationController?.pushViewController(controller, animated: true)
    }
    

}
