//
//  AvailableRoomsListRoomCapacityPicker.swift
//  RoomBooking
//
//  Created by VI_Business on 06/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

/**
 * Gathers infotmation about desired room capacity
 */
class AvailableRoomsListRoomCapacityPicker {
    let capacitySelectedSignal = PublishSubject<Int>()
    var disposeBag: DisposeBag!
    let viewModel: AvailableRoomsListViewModel
    
    init(viewModel: AvailableRoomsListViewModel) {
        self.viewModel = viewModel
    }
    
    func presentPicker() {
        disposeBag = DisposeBag()
        let controller = UIAlertController.init(title: "SelectCapacity".localized, message: nil, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK".localized, style: .default) { [weak self, weak controller] _ in
            let strongSelf = self!
            let strongController = controller!
            let result = Int(strongController.textFields!.last!.text!)!
            strongSelf.viewModel.roomCapacityFilter.onNext(result)
        }
        
        let resetAction = UIAlertAction(title: "Reset".localized, style: .default) { [weak self] _ in
            let strongSelf = self!
            strongSelf.viewModel.roomCapacityFilter.onNext(nil)
        }
        
        controller.addTextField { [weak self] (textField) in
            let strongSelf = self!
            textField.keyboardType = UIKeyboardType.numberPad
            textField.rx.text.map {$0 != nil && !$0!.isEmpty}.bind(to: okAction.rx.isEnabled).disposed(by: strongSelf.disposeBag)
        }
        
        controller.addAction(okAction)
        controller.addAction(resetAction)
        
        UIViewController.topmostViewController.present(controller, animated: true, completion: nil)
    }
}
