//
//  RootServicesDomain.swift
//
//  Created by VI_Business on 03/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

/**
 *  Collection of application-wide services
 */
class RootServicesDomain {
    let roomBookingNetworkService: RoomBookingNetworkService
    let alertMessagePresenter: AppAlertMessagePresenter
    let imageDownloader: NetworkImageDownloader
    
    static let sharedInstance = RootServicesDomain()
    
    private init() {
        roomBookingNetworkService = RoomBookingNetworkService()
        alertMessagePresenter = AppAlertMessagePresenter()
        imageDownloader = NetworkImageDownloader()
    }
}
