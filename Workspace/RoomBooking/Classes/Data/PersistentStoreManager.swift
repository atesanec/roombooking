//
//  PersistentStoreManager
//  RoomBooking
//
//  Created by VI_Business on 3/12/2018.
//  Copyright © 2018 Test. All rights reserved.
//

import UIKit
import CoreData

typealias PersistentStoreManagerHandler = () -> Void
typealias PersistentStoreManagerListHandler = ([NSManagedObject]?) -> Void

class PersistentStoreManager {
    
    /// Shared instance
    static let sharedManager = PersistentStoreManager()
    var mainQueueContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    private static let persistentStoreModelName = "RoomBooking"
    private static var persistentStorePath: URL {
        return NSPersistentContainer.defaultDirectoryURL().appendingPathComponent(self.persistentStoreModelName).appendingPathExtension("db")
    }
    
    private let persistentContainer: NSPersistentContainer
    
    private init() {
        self.persistentContainer = NSPersistentContainer(name: PersistentStoreManager.persistentStoreModelName)
        
        let storeURL = PersistentStoreManager.persistentStorePath
        if !checkStoreCompatibility() {
            try! FileManager.default.removeItem(at: storeURL)
        }

        let storeDesc = NSPersistentStoreDescription(url: storeURL)
        storeDesc.shouldMigrateStoreAutomatically = false
        self.persistentContainer.persistentStoreDescriptions = [storeDesc]
        self.persistentContainer.loadPersistentStores { [weak self] (description, error) in
            assert(error == nil, error.debugDescription)
            self?.mainQueueContext.automaticallyMergesChangesFromParent = true
        }
    }
    
    func createPrivateTrackingContext() -> NSManagedObjectContext {
        return self.persistentContainer.newBackgroundContext()
    }
    
    func entitiesInContext<T>(_ entities: [T], in context: NSManagedObjectContext) -> [T] where T: NSManagedObject {
        var resultEntities: [T] = []
        for entity in entities {
            if let obj = self.entityInContext(entity, in: context) {
                resultEntities.append(obj)
            }
        }
        
        return resultEntities
    }
    
    func entityInContext<T>(_ entity: T, in context: NSManagedObjectContext) -> T? where T: NSManagedObject {
        var result: NSManagedObject? = nil
        context.performAndWait({() -> Void in
            if entity.objectID.isTemporaryID {
                guard let _ = try? entity.managedObjectContext?.obtainPermanentIDs(for: [entity]) else {
                    return
                }
            }
            
            result = try? context.existingObject(with: entity.objectID)
        })
        return result as? T
    }
 
    // MARK:- Private methods
    
    private func checkStoreCompatibility() -> Bool {
        let storeURL = PersistentStoreManager.persistentStorePath
        if !FileManager.default.fileExists(atPath: storeURL.path) {
            return true
        }
        
        let sourceMetadata = try? NSPersistentStoreCoordinator.metadataForPersistentStore(ofType: NSSQLiteStoreType, at: storeURL, options: nil)
        if sourceMetadata == nil {
            return false
        }
        
        let model = NSManagedObjectModel(contentsOf: Bundle.main.url(forResource: PersistentStoreManager.persistentStoreModelName,
                                                                     withExtension: "momd")!)
     
        return model!.isConfiguration(withName: nil, compatibleWithStoreMetadata: sourceMetadata!)
    }
}
