//
//  BookingEventMember.swift
//  RoomBooking
//
//  Created by VI_Business on 07/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

/**
 *  Member of room booking event
 */
class RoomBookingEventMember {
    var name: String?
    var phone: String?
    var email: String?
    
    func toBookingRequestParameters() -> [String: Any] {
        return [
            "name": name!,
            "number": phone!,
            "email": email!
        ]
    }
}
