//
//  RoomBookingNetworkServiceError.swift
//  RoomBooking
//
//  Created by VI_Business on 08/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

class RoomBookingNetworkServiceError: RoomBookingServiceResponseMappable {
    var text: String!
    var code: Int!

    static func objectForMapping(fromJSON: Any, context: RoomBookingServiceResponseContext, parent: Any?) -> RoomBookingServiceResponseMappable {
        return RoomBookingNetworkServiceError()
    }
    
    func map(fromJSON: Any, context: RoomBookingServiceResponseContext, parent: Any?) {
        let jsonInfo = fromJSON as! [String: Any]
        
        text = (jsonInfo["text"] as! String)
        code = (jsonInfo["code"] as! Int)
    }
}
