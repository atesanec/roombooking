//
//  CreateBookingResponse.swift
//  RoomBooking
//
//  Created by VI_Business on 08/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

/**
 *  Response for create booking event request
 */
class CreateBookingResponse: RoomBookingServiceResponseMappable {
    var successful: Int!
    
    static func objectForMapping(fromJSON: Any, context: RoomBookingServiceResponseContext, parent: Any?) -> RoomBookingServiceResponseMappable {
        return CreateBookingResponse()
    }
    
    func map(fromJSON: Any, context: RoomBookingServiceResponseContext, parent: Any?) {
        let jsonInfo = fromJSON as! [String: Any]
        
        successful = (jsonInfo["success"] as! Int)
    }
}
