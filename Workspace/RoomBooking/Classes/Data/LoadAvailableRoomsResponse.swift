//
//  LoadAvailableRoomsResponse.swift
//  RoomBooking
//
//  Created by VI_Business on 03/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

class LoadAvailableRoomsResponse: RoomBookingServiceResponseMappable {
    var availableRooms = [RoomForDayEntity]()
    
    static func objectForMapping(fromJSON: Any, context: RoomBookingServiceResponseContext, parent: Any?) -> RoomBookingServiceResponseMappable {
        return LoadAvailableRoomsResponse()
    }
    
    func map(fromJSON: Any, context: RoomBookingServiceResponseContext, parent: Any?) {
        let jsonRoot = fromJSON as! [Any]
        
        for item in jsonRoot {
            let room = RoomForDayEntity.objectForMapping(fromJSON: item, context: context, parent: self) as! RoomForDayEntity
            room.map(fromJSON: item, context: context, parent: self)
            availableRooms.append(room)
        }
    }
}
