//
//  RoomForDayEntity+Mapping.swift
//  RoomBooking
//
//  Created by VI_Business on 03/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import CoreData

enum RoomForDayEntityAttributes: String {
    case name
    case startOfDayDate
}

extension RoomForDayEntity: RoomBookingServiceResponseMappable {
    
    
    static func objectForMapping(fromJSON: Any, context: RoomBookingServiceResponseContext, parent: Any?) -> RoomBookingServiceResponseMappable {
        let name = fetchRoomName(fromJSON: fromJSON)
        let dayDate = fetchRoomDayDate(context: context)
        
        let request = NSFetchRequest<RoomForDayEntity>(entityName: "RoomForDayEntity")
        request.predicate = NSPredicate(format: "%K == %@ AND %K == %@",
                                        RoomForDayEntityAttributes.name.rawValue,
                                        name,
                                        RoomForDayEntityAttributes.startOfDayDate.rawValue,
                                        dayDate as NSDate
        )
        
        let results = try! context.managedObjectContext.fetch(request)
        var mappedObject: NSManagedObject!
        if results.isEmpty {
            mappedObject = NSEntityDescription.insertNewObject(forEntityName: "RoomForDayEntity", into: context.managedObjectContext)
            populateTimeSlots(entity: mappedObject as! RoomForDayEntity, context: context)
        } else {
            assert(results.count == 1)
            mappedObject = results.first!
        }
        
        context.mappedEntities.insert(mappedObject)
        return mappedObject as! RoomForDayEntity
    }
    
    func map(fromJSON: Any, context: RoomBookingServiceResponseContext, parent: Any?) {
        let jsonInfo = fromJSON as! [String: Any]
        
        self.name = RoomForDayEntity.fetchRoomName(fromJSON: fromJSON)
        self.startOfDayDate = RoomForDayEntity.fetchRoomDayDate(context: context)
        self.equipment = (jsonInfo["equipment"] as! [String])
        self.sizeSquareMeters = RoomForDayEntity.fetchRoomSize(fromJSON: fromJSON)
        self.roomCapacity = jsonInfo["capacity"] as! Int16
        self.location = (jsonInfo["location"] as! String)
        
        self.images = (jsonInfo["images"] as! [String]).map {RoomBookingNetworkService.baseURL.appendingPathComponent($0)}
        
        let freeTimeSlotsInfo = jsonInfo["avail"] as! [String]
        let analytics = RoomForDayTimeAnalyticsAdapter(roomEntity: self)
        for item in freeTimeSlotsInfo {
            let (startOffset, endOffset) = RoomForDayEntity.fetchSlotInterval(fromJSON: item)
            // Map slots only once, so our local booking info won't be overwritten
            analytics.timeSlotsWithinTimeInterval(from: startOffset, to: endOffset).forEach { slot in
                if !slot.isMapped {
                    slot.isVacant = true
                    slot.isMapped = true
                }
            }
        }
        
        (timeSlots!.array as! [RoomForDayTimeSlotEntity]).forEach { slot in
            if !slot.isMapped {
                slot.isVacant = false
                slot.isMapped = true
            }
        }
    }
    
    private static func fetchRoomName(fromJSON: Any) -> String {
        let jsonInfo = fromJSON as! [String: Any]
        return jsonInfo["name"] as! String
    }
    
    private static func fetchRoomDayDate(context: RoomBookingServiceResponseContext) -> Date {
        let serverDate = Date(timeIntervalSince1970: context.requestParameters[RoomBookingNetworkService.Params.date.rawValue] as! TimeInterval)
        let globalDate = serverDate.roomBookingServiceTimeToGlobalTime()
        
        return globalDate
    }
    
    private static func fetchRoomSize(fromJSON: Any) -> Int16 {
        let jsonInfo = fromJSON as! [String: Any]
        let rawSizeText = jsonInfo["size"] as! String
        
        return Int16(rawSizeText.replacingOccurrences(of: "m²", with: ""))!
    }
    
    private static func populateTimeSlots(entity: RoomForDayEntity, context: RoomBookingServiceResponseContext) {
        let start = RoomBookingNetworkService.firstTimeSlotStartDayOffsetSeconds
        let end = RoomBookingNetworkService.lastTimeSlotEndDayOffsetSeconds
        let by = RoomBookingNetworkService.timeSlotStepSeconds
        for startOffset in stride(from: start, to: end, by: by) {
            let mappedObject = NSEntityDescription.insertNewObject(forEntityName: "RoomForDayTimeSlotEntity",
                                                                   into: context.managedObjectContext) as! RoomForDayTimeSlotEntity
            
            mappedObject.fromTimeOffset = startOffset
            mappedObject.toTimeOffset = startOffset + by
            entity.addToTimeSlots(mappedObject)
        }
    }
    
    private static func fetchSlotInterval(fromJSON: Any) -> (fromTimeOffset: TimeInterval, toTimeOffset: TimeInterval) {
        let intervalText = fromJSON as! String
        let timeComponentsText = intervalText.split(separator: Character("-")).map {$0.trimmingCharacters(in: NSCharacterSet.whitespaces)}
        assert(timeComponentsText.count == 2)
        
        // HH:mm converting to time offset since start of a day
        let timeOffsets = timeComponentsText.map { (component) -> TimeInterval in
            let hoursAndMinutes = component.split(separator: ":").map {TimeInterval($0)!}
            return hoursAndMinutes[0] * 3600 + hoursAndMinutes[1] * 60
        }
        
        return (fromTimeOffset: timeOffsets[0], toTimeOffset: timeOffsets[1])
    }
}
