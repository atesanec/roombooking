//
//  RoomBookingEvent.swift
//  RoomBooking
//
//  Created by VI_Business on 08/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

/**
 *  Room booking event
 */
class RoomBookingEvent {
    let title: String
    let eventDescription: String
    let room: RoomForDayEntity
    let timeSlots: [RoomForDayTimeSlotEntity]
    let members: [RoomBookingEventMember]

    init(title: String, description: String, room: RoomForDayEntity, timeSlots: [RoomForDayTimeSlotEntity], members: [RoomBookingEventMember]) {
        self.title = title
        self.eventDescription = description
        self.room = room
        self.timeSlots = timeSlots
        self.members = members
    }
    
    func toBookingRequestParameters() -> [String: Any] {
        var result = [String: Any]()
        let startOfDay = room.startOfDayDate!.globalTimeToRoomBookingServiceTime().timeIntervalSince1970
        result["booking"] = [
            "date": startOfDay,
            "time_start": startOfDay + timeSlots.first!.fromTimeOffset,
            "time_end": startOfDay + timeSlots.last!.toTimeOffset,
            "title": title,
            "description": eventDescription,
            "room": room.name!
        ]
        
        result["passes"] = members.map {$0.toBookingRequestParameters()}
        
        return result
    }
}
