//
//  RoomForDayTimeSlotEntity+ConvenienceAccessors.swift
//  RoomBooking
//
//  Created by VI_Business on 05/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import CoreData

enum RoomForDayTimeSlotAttribute: String {
    case room
}

extension RoomForDayTimeSlotEntity {
    static func slotsForRoomFetchRequest(room: RoomForDayEntity) -> NSFetchRequest<RoomForDayTimeSlotEntity> {
        let request = NSFetchRequest<RoomForDayTimeSlotEntity>(entityName: "RoomForDayTimeSlotEntity")
        request.predicate = NSPredicate(format: "%K == %@",
                                        RoomForDayTimeSlotAttribute.room.rawValue,
                                        room)
        
        return request
    }
    
    var slotDuration: TimeInterval {
        return toTimeOffset - fromTimeOffset
    }
}
