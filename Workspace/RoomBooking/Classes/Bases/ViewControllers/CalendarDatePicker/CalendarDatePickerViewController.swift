//
//  CalendarDatePickerViewController.swift
//  RoomBooking
//
//  Created by VI_Business on 05/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import FSCalendar
import RxSwift

/**
 *  Picker for the date from calendar
 */
class CalendarDatePickerViewController: UIViewController, FSCalendarDelegate, FSCalendarDataSource {
    let dateSelectedSignal = PublishSubject<Date>()
    let pickerCancelledSignal = PublishSubject<Void>()
    
    private let disposeBag = DisposeBag()
    
    func presentFromTopmostController() {
        let rootController = UINavigationController(rootViewController: self)
        UIViewController.topmostViewController.present(rootController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        view.backgroundColor = .white
        
        let calendar = FSCalendar(frame: view.bounds)
        calendar.delegate = self
        calendar.dataSource = self
        view.addSubview(calendar)
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: nil)
        navigationItem.leftBarButtonItem!.rx.tap.subscribe({ [weak self] _ in
            self?.pickerCancelledSignal.onNext(())
        }).disposed(by: disposeBag)
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        dateSelectedSignal.onNext(date)
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        calendar.frame = view.bounds
    }
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }
}
