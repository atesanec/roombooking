//
//  LoadedListPlaceholderViewController.swift
//
//  Created by VI_Business on 06/09/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

/**
 *  Placeholder view controller for the list being loaded
 */
class LoadedListPlaceholderViewController : UIViewController {
    /// Is list currently loading
    let isLoadingObserver = BehaviorSubject<Bool>(value: false)
    /// Number of items in the list
    let itemCountObserver = BehaviorSubject<Int>(value: 0)
    /// Emits whenever reload button is tapped
    let reloadButtonTappedObservable = PublishSubject<Void>()
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var reloadContainerView: UIView!
    @IBOutlet weak var reloadButton: UIButton!
    @IBOutlet weak var reloadCaption: UILabel!
    
    private let disposeBag = DisposeBag()
    
    convenience init() {
        self.init(nibName: "LoadedListPlaceholderViewController", bundle: nil)
    }
    
    override func viewDidLoad() {
        setupObservations()
        
        reloadButton.setTitle("TapToRetry".localized, for: .normal)
        reloadCaption.text = "NoDataAvailable".localized
    }
    
    private func setupObservations() {
        itemCountObserver.map({$0 > 0}).bind(to: view.rx.isHidden).disposed(by: disposeBag)
        isLoadingObserver.bind(to: reloadContainerView.rx.isHidden).disposed(by: disposeBag)
        isLoadingObserver.map({!$0}).bind(to: activityIndicator.rx.isHidden).disposed(by: disposeBag)
        
        reloadButton.rx.tap.bind(to: reloadButtonTappedObservable).disposed(by: disposeBag)
    }
}
