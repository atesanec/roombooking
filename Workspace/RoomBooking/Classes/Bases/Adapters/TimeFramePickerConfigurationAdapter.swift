//
//  TimeFramePickerConfigurationAdapter.swift
//  RoomBooking
//
//  Created by VI_Business on 07/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

class TimeFramePickerConfigurationAdapter {
    private let timeFrameView: TimeFramePickerView
    
    init(timeFrameView: TimeFramePickerView) {
        self.timeFrameView = timeFrameView
    }
    
    func configure(withRoomItem room: RoomForDayEntity) {
        let timeSlots = room.timeSlots!.array as! [RoomForDayTimeSlotEntity]
        timeFrameView.minTimeOffsetSeconds = timeSlots.first!.fromTimeOffset
        timeFrameView.maxTimeOffsetSeconds = timeSlots.last!.toTimeOffset
        
        let analytics = RoomForDayTimeAnalyticsAdapter(roomEntity: room)
        let isAvailable = analytics.vacantSlotsForWholeInterval(from: timeFrameView.selectedTimeInterval.from,
                                                                to: timeFrameView.selectedTimeInterval.to) != nil
        timeFrameView.intervalFillColor = isAvailable ? .turbo : .tango
    }
}
