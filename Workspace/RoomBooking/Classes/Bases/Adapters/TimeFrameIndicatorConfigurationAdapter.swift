//
//  TimeFrameIndicatorRoomForDayConfigurationAdapter.swift
//  RoomBooking
//
//  Created by VI_Business on 06/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

class TimeFrameIndicatorConfigurationAdapter {
    private let timeFrameView: TimeFrameIndicatorView
    
    init(timeFrameView: TimeFrameIndicatorView) {
        self.timeFrameView = timeFrameView
    }

    func configure(withRoomItem room: RoomForDayEntity) {
        let timeSlots = room.timeSlots!.array as! [RoomForDayTimeSlotEntity]
        timeFrameView.minTimeOffsetSeconds = timeSlots.first!.fromTimeOffset
        timeFrameView.maxTimeOffsetSeconds = timeSlots.last!.toTimeOffset
        timeFrameView.axisMarksOffsetSeconds = 60 * 60
        
        var colorOffsets = [TimeInterval: UIColor]()
        for slot in timeSlots {
            colorOffsets[slot.fromTimeOffset] = slot.isVacant ? UIColor.greenHaze : UIColor.alizarinCrimson
        }
        timeFrameView.colorStartOffsets = colorOffsets
    }
}
