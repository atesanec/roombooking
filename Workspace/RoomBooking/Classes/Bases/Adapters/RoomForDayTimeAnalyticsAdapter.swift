//
//  RoomForDayTimeAnalyticsAdapter.swift
//  RoomBooking
//
//  Created by VI_Business on 05/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

/**
 *  Set of logic for room time slot analysis
 */
class RoomForDayTimeAnalyticsAdapter {
    private static let secondsInHour: TimeInterval = 60 * 60
    private let roomEntity: RoomForDayEntity
    
    init(roomEntity: RoomForDayEntity) {
        self.roomEntity = roomEntity
    }
    
    var isAvailableNextHour: Bool {
        return continuousAvailableTimeInterval(fromDate: Date()) >= RoomForDayTimeAnalyticsAdapter.secondsInHour
    }
    
    var totalSlotTimeInterval: (from: TimeInterval, to: TimeInterval) {
        let timeSlots = roomEntity.timeSlots!.array as! [RoomForDayTimeSlotEntity]
        return (from: timeSlots.first!.fromTimeOffset, to: timeSlots.last!.toTimeOffset)
    }
    
    func continuousAvailableTimeInterval(fromDate: Date) -> TimeInterval {
        let timeSlots = roomEntity.timeSlots!.array as! [RoomForDayTimeSlotEntity]
        let startOffset = timeOffsetSinceStartOfDay(forDate: fromDate)
        
        var intervalStarted = false
        var totalDuration: TimeInterval = 0
        for slot in timeSlots {
            if slot.isVacant && slot.fromTimeOffset <= startOffset && slot.toTimeOffset > startOffset {
                intervalStarted = true
            }
            
            if !intervalStarted {
                continue
            }
            
            if !slot.isVacant {
                break
            }
        
            totalDuration += slot.toTimeOffset - slot.fromTimeOffset
        }
        
        return totalDuration
    }
    
    func timeSlotsWithinTimeInterval(from: TimeInterval, to: TimeInterval) -> [RoomForDayTimeSlotEntity] {
        let timeSlots = roomEntity.timeSlots!.array as! [RoomForDayTimeSlotEntity]
        return timeSlots.filter {$0.fromTimeOffset >= from && $0.fromTimeOffset <= to || $0.toTimeOffset <= to && $0.toTimeOffset >= from}
    }
    
    func vacantSlotsForWholeInterval(from: TimeInterval, to: TimeInterval) -> [RoomForDayTimeSlotEntity]? {
        let timeSlots = timeSlotsWithinTimeInterval(from: from, to: to)
        
        if !timeSlots.allSatisfy {$0.isVacant} {
            return nil
        }
        
        return timeSlots
    }
    
    private func timeOffsetSinceStartOfDay(forDate: Date) -> TimeInterval {
        let serviceDate = forDate.localTimeToRoomBookingServiceTime()
        return serviceDate.timeIntervalSince(serviceDate.truncatedToDay(fromTimeZone: Date.roomBookingServiceTimeZone))
    }
}
