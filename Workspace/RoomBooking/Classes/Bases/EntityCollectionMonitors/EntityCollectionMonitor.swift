//
//  EntityCollectionMonitor.swift
//
//  Created by VI_Business on 18/07/2018.
//  Copyright © 2018 Test. All rights reserved.
//

import Foundation
import CoreData
import RxSwift

/// Monitor that triggers callback after entity collection deacribed by fetch request changes
class EntityCollectionMonitor<T> where T: NSManagedObject {
    private let fetchRequest: NSFetchRequest<T>
    private let managedObjectContext: NSManagedObjectContext
    private var disposeBag = DisposeBag()
    private(set) var currentEntities = Set<T>()
    
    let entityCollection = BehaviorSubject<[T]>(value: [])

    init(fetchRequest: NSFetchRequest<T>, context: NSManagedObjectContext = PersistentStoreManager.sharedManager.mainQueueContext) {
        self.fetchRequest = fetchRequest
        self.managedObjectContext = context
    }
    
    func startMonitoring() {
        self.refetchEntityCollection()
        
        NotificationCenter.default.rx.notification(NSNotification.Name.NSManagedObjectContextObjectsDidChange, object: managedObjectContext)
            .subscribe { [weak self] (event) in
                self?.handleCollectionChange(notification: event.element!)
            }.disposed(by: self.disposeBag)
    }
    
    func stopMonitoring() {
        disposeBag = DisposeBag()
    }
    
    // Mark:- Private methods
    private func handleCollectionChange(notification: Notification) {
        let userInfo = notification.userInfo!
        var changedObjects = Set<NSManagedObject>()
        for item in [NSInsertedObjectsKey, NSUpdatedObjectsKey, NSRefreshedObjectsKey] {
            if let objects = userInfo[item] {
                changedObjects.formUnion(objects as! Set<NSManagedObject>)
            }
        }

        let deletedObjects = userInfo[NSDeletedObjectsKey] as? Set<NSManagedObject>
        if !isCollectionAffected(changedEntities: changedObjects, deletedEntities: deletedObjects) {
            return
        }
        
        updateEntityCollection(changedEntities: changedObjects, deletedEntities: deletedObjects)
    }
    
    private func isCollectionAffected(changedEntities: Set<NSManagedObject>, deletedEntities: Set<NSManagedObject>?) -> Bool {
        for item in changedEntities {
            guard let entity = item as? T else {
                continue
            }
            
            let conformsToRequest = fetchRequest.predicate!.evaluate(with: entity)
            if conformsToRequest || currentEntities.contains(entity) {
                return true
            }
        }
        
        guard let deletedObjects = deletedEntities else {
            return false
        }
        
        return deletedObjects.intersection(currentEntities).count > 0
    }
    
    private func refetchEntityCollection() {
        let fetchResult = try! managedObjectContext.fetch(fetchRequest)
        currentEntities = Set<T>(fetchResult)
        emitUpdatedEntityCollection()
    }
    
    private func updateEntityCollection(changedEntities: Set<NSManagedObject>, deletedEntities: Set<NSManagedObject>?) {
        for item in changedEntities {
            guard let entity = item as? T else {
                continue
            }
            
            let conformsToRequest = fetchRequest.predicate!.evaluate(with: entity)
            if conformsToRequest {
                currentEntities.insert(entity)
            } else {
                currentEntities.remove(entity)
            }
        }
        
        if let deletedItems = deletedEntities {
            currentEntities.subtract(deletedItems.compactMap{$0 as? T})
        }
        
        emitUpdatedEntityCollection()
    }
    
    private func emitUpdatedEntityCollection() {
        var entityList = [T](currentEntities)
        if let sortDesc = fetchRequest.sortDescriptors, sortDesc.count > 0 {
            entityList = [T](currentEntities).sorted { (one, two) -> Bool in
                for item in sortDesc {
                    let result = item.compare(one, to: two)
                    if result == ComparisonResult.orderedSame {
                        continue
                    }
                    
                    return result == ComparisonResult.orderedAscending
                }
                
                return true
            }
        }
        
        entityCollection.onNext(entityList)
    }
}
