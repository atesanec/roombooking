//
//  RoomTimeSlotsMonitor.swift
//  RoomBooking
//
//  Created by VI_Business on 08/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

/**
 *  Monitors changes in room time slots
 */
class RoomTimeSlotsMonitor {
    private let entityMonitor: EntityCollectionMonitor<RoomForDayTimeSlotEntity>
    let timeSlotsUpdatedSignal = PublishSubject<[RoomForDayTimeSlotEntity]>()
    let disposeBag = DisposeBag()
    
    init(room: RoomForDayEntity) {
        let request = RoomForDayTimeSlotEntity.slotsForRoomFetchRequest(room: room)
        entityMonitor = EntityCollectionMonitor<RoomForDayTimeSlotEntity>(fetchRequest: request)
        entityMonitor.entityCollection.bind(to: timeSlotsUpdatedSignal).disposed(by: disposeBag)
    }
    
    public func startMonitoring() {
        entityMonitor.startMonitoring()
    }
    
    public func stopMonitoring() {
        entityMonitor.stopMonitoring()
    }
}
