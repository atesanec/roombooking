//
//  RoomBookingServiceResponseMapper.swift
//  RoomBooking
//
//  Created by VI_Business on 03/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

/**
 *  Maps Responses, transforming them into specified entity
 */
class RoomBookingServiceResponseMapper: NSObject {
    static private let mappingQueue: DispatchQueue = DispatchQueue(label: "MappingQueue")
    
    /// Map response into entity of the specified type
    func mapJSON<T>(json: Any,
                    requestParameters: [String: Any],
                    response: HTTPURLResponse,
                    completion: @escaping((T) -> ())) where T: RoomBookingServiceResponseMappable {
        RoomBookingServiceResponseMapper.mappingQueue.async {
            let managedObjectContext = PersistentStoreManager.sharedManager.createPrivateTrackingContext()
            let mappingContext = RoomBookingServiceResponseContext(context: managedObjectContext, requestParameters: requestParameters,
                                                                   response: response)
            managedObjectContext.performAndWait {
                let result = T.objectForMapping(fromJSON: json, context: mappingContext, parent: nil)
                result.map(fromJSON: json, context: mappingContext, parent: self)
                
                try! mappingContext.managedObjectContext.save()
                DispatchQueue.main.async {
                    completion(result as! T)
                }
            }
        }
    }
}
