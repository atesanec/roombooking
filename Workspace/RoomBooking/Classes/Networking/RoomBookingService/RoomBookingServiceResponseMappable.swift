//
//  RoomBookingServiceResponseMappable.swift
//  RoomBooking
//
//  Created by VI_Business on 03/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

/**
 *  Generic protocol for mapped entity
 */
protocol RoomBookingServiceResponseMappable {
    /// Target obejct for mapping
    static func objectForMapping(fromJSON: Any, context: RoomBookingServiceResponseContext, parent: Any?) -> RoomBookingServiceResponseMappable
    /// Perform object mapping from JSON
    func map(fromJSON: Any, context: RoomBookingServiceResponseContext, parent: Any?)
}
