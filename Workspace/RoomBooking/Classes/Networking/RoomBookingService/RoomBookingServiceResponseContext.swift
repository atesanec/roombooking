//
//  RoomBookingServiceResponseContext.swift
//  RoomBooking
//
//  Created by VI_Business on 03/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import CoreData

/**
 *  Context for response mapping
 */
class RoomBookingServiceResponseContext: NSObject {
    let managedObjectContext: NSManagedObjectContext
    let response: HTTPURLResponse
    let requestParameters: [String: Any]
    var mappedEntities = Set<NSManagedObject>()
    
    init(context: NSManagedObjectContext, requestParameters: [String: Any], response: HTTPURLResponse) {
        self.managedObjectContext = context
        self.response = response
        self.requestParameters = requestParameters
    }
}
