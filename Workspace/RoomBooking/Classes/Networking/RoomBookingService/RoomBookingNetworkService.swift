//
//  RoomBookingNetworkService.swift
//  RoomBooking
//
//  Created by VI_Business on 03/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import Foundation
import RxSwift
import CoreData
import Alamofire

class RoomBookingNetworkService {
    enum ServiceError: Error, LocalizedError {
        case invalidResponse
        case badStatusCode(code: Int)
        case emptyData
        case invalidFormat
        case serviceError(error: RoomBookingNetworkServiceError)
        
        var errorDescription: String? {
            switch self {
            case .invalidResponse, .invalidFormat:
                return "InvalidResponseError".localized
            
            case .badStatusCode(let code):
                return String(format: "InvalidHttpResponseError", code)
                
            case .emptyData:
                return "EmptyResponseError".localized
                
            case .serviceError(let error):
                return String(format: "ServerLogicError".localized, error.text, error.code)
            }
        }
    }
    
    enum Params : String {
        case date
        case query = "q"
    }
    
    enum Paths : String {
        case loadAvailableRooms = "getrooms"
        case createBooking = "sendpass"
    }
    
    static let baseURL: URL = URL(string: "https://challenges.1aim.com/roombooking_app")!
    static let timeSlotStepSeconds: TimeInterval = 15 * 60
    static let firstTimeSlotStartDayOffsetSeconds: TimeInterval = 7 * 60 * 60
    static let lastTimeSlotEndDayOffsetSeconds: TimeInterval = 19 * 60 * 60
    
    private let mapper = RoomBookingServiceResponseMapper()
    
    func loadAvailableRooms(forDate: Date) -> Observable<[RoomForDayEntity]> {
        return Observable<[RoomForDayEntity]>.create { (observer) -> Disposable in
            let params = [
                Params.date.rawValue : forDate.localTimeToRoomBookingServiceTime()
                    .truncatedToDay(fromTimeZone: Date.roomBookingServiceTimeZone).timeIntervalSince1970
            ]
            
            let path = RoomBookingNetworkService.makeURL(path: .loadAvailableRooms)
            let task = Alamofire.request(path, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil)
                .responseData(completionHandler: { [weak self] (info) in
                    let strongSelf = self
                    strongSelf?.performMapping(requestParams: params,
                                               response: info,
                                               success: { (respose: LoadAvailableRoomsResponse) in
                                                    let manager = PersistentStoreManager.sharedManager
                                                    let result = manager.entitiesInContext(respose.availableRooms, in: manager.mainQueueContext)
                                                    observer.onNext(result)
                                                    observer.onCompleted()
                    },
                                               failure: { (error) in
                                                    observer.onError(error)
                    })
                })
            
            return Disposables.create {
                task.cancel()
            }
        }
    }
    
    func createBookingEvent(event: RoomBookingEvent) -> Observable<Int> {
        return Observable<Int>.create { (observer) -> Disposable in
            let params = event.toBookingRequestParameters()
            let path = RoomBookingNetworkService.makeURL(path:.createBooking)
            let task = Alamofire.request(path, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil)
                .responseData(completionHandler: { [weak self] (info) in
                    let strongSelf = self
                    strongSelf?.performMapping(requestParams: params,
                                               response: info,
                                               success: { [weak self] (respose: CreateBookingResponse) in
                                                guard let strongSelf = self else {
                                                    return
                                                }
                                                
                                                strongSelf.markOccupied(slots: event.timeSlots)
                                                observer.onNext(respose.successful!)
                                                observer.onCompleted()
                    },
                                               failure: { (error) in
                                                observer.onError(error)
                    })
                })
            
            return Disposables.create {
                task.cancel()
            }
        }
    }
    
    private func performMapping<T>(requestParams: [String: Any],
                                   response info: DataResponse<Data>,
                                   success: @escaping ((T) -> ()),
                                   failure: @escaping ((Error) -> ()) ) where T: RoomBookingServiceResponseMappable {
        
        if let resultError = info.error {
            failure(resultError)
        }
        
        guard let httpResponse = info.response else {
            failure(ServiceError.invalidResponse)
            return
        }
        
        if httpResponse.statusCode != 200 {
            failure(ServiceError.badStatusCode(code: httpResponse.statusCode))
            return
        }
        
        guard let responseData = info.data else {
            failure(ServiceError.emptyData)
            return
        }
        
        guard let jsonObject = try? JSONSerialization.jsonObject(with: responseData, options: []) else {
            failure(ServiceError.invalidFormat)
            return
        }
        
        if let jsonInfo = jsonObject as? [String: Any], let errorInfo = jsonInfo["error"] {
            self.mapper.mapJSON(json: errorInfo,
                                requestParameters: requestParams,
                                response: httpResponse,
                                completion: { (serviceError: RoomBookingNetworkServiceError) in
              failure(ServiceError.serviceError(error: serviceError))
            })
            return
        }
        
        self.mapper.mapJSON(json: jsonObject, requestParameters: requestParams, response: httpResponse, completion: success)
    }
    
    private static func makeURL(path: Paths) -> URL {
        return self.baseURL.appendingPathComponent(path.rawValue)
    }
    
    private func markOccupied(slots: [RoomForDayTimeSlotEntity]) {
        let manager = PersistentStoreManager.sharedManager
        let context = manager.createPrivateTrackingContext()
        context.performAndWait {
            let convertedSlots = manager.entitiesInContext(slots, in: context)
            convertedSlots.forEach {$0.isVacant = false}
            
            try! context.save()
        }
    }
}
