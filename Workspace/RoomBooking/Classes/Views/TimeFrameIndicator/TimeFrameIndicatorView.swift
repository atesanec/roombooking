//
//  TimeFrameIndicatorView.swift
//  RoomBooking
//
//  Created by VI_Business on 05/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

/**
 *  Shows specified time frame (with time offsets since start of a day) as a stripe with labels
 */
class TimeFrameIndicatorView: UIView {
    enum Style {
        case minimal
        case full
    }
    
    var minTimeOffsetSeconds: TimeInterval = 0 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    var maxTimeOffsetSeconds: TimeInterval = 60 * 60 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    var axisMarksOffsetSeconds: TimeInterval = 15 * 60 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    var defaultFillColor: UIColor = .red {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    var style: Style = .full {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    var colorStartOffsets = [TimeInterval: UIColor]() {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    private static let axisMarkWidth: CGFloat = 2.0
    private static let axisLabelFont = UIFont.systemFont(ofSize: 12)
    private static let timeFrameBarInset: CGFloat = 16
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        clipsToBounds = false
        contentMode = .redraw
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        drawTimeFrame()
        drawAxes()
    }
    
    private func drawTimeFrame() {
        let barFrame = timeScaleBarFrame()
        let barHeight: CGFloat = barFrame.height
        // Stroke is being drawn from both sides of a line, thus coordinates should be adjusted
        let lineOffsetY: CGFloat = barFrame.origin.y + barHeight * 0.5
        
        let context = UIGraphicsGetCurrentContext()!
        context.setLineWidth(barHeight)
        
        var lastOffsetX: CGFloat = barFrame.minX
        context.move(to: CGPoint(x: lastOffsetX, y: lineOffsetY))
        context.setStrokeColor(defaultFillColor.cgColor)
        for currentOffset in colorStartOffsets.sorted(by: {$0.key < $1.key}) {
            let currentX = timeOffsetToCanvasOffset(timeOffset: currentOffset.key)
            let currentColor = currentOffset.value
            
            if currentX == lastOffsetX {
                lastOffsetX = currentX
                context.setStrokeColor(currentColor.cgColor)
                continue
            }
            
            let targetPoint = CGPoint(x: currentX, y: lineOffsetY)
            context.addLine(to: targetPoint)
            context.strokePath()
            
            lastOffsetX = currentX
            context.move(to: targetPoint)
            context.setStrokeColor(currentColor.cgColor)
        }
        
        let finalLinePoint = CGPoint(x: barFrame.maxX, y: lineOffsetY)
        context.addLine(to: finalLinePoint)
        context.strokePath()
    }
    
    private func timeOffsetToCanvasOffset(timeOffset: TimeInterval) -> CGFloat {
        assert(timeOffset >= minTimeOffsetSeconds)
        
        let barFrame = timeScaleBarFrame()
        return ceil(CGFloat((timeOffset - minTimeOffsetSeconds) / (maxTimeOffsetSeconds - minTimeOffsetSeconds))
            * barFrame.width + barFrame.minX)
    }
    
    func timeScaleBarFrame() -> CGRect {
        switch style {
        case .minimal:
            return CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
            
        case .full:
            let inset = TimeFrameIndicatorView.timeFrameBarInset
            return CGRect(x: inset, y: bounds.height * 0.5,
                          width: bounds.width - 2 * inset, height: bounds.height * 0.5)
        }
    }
    
    private func drawAxes() {
        var currentAxisMarkTimeOffset = minTimeOffsetSeconds
        var axisIndex = 0
        while currentAxisMarkTimeOffset <= maxTimeOffsetSeconds  {
            let markPositionX = timeOffsetToCanvasOffset(timeOffset: currentAxisMarkTimeOffset)
            drawAxisMark(offsetX: markPositionX)
            if style != .full {
                currentAxisMarkTimeOffset += axisMarksOffsetSeconds
                axisIndex += 1
                continue
            }
            
            let labelText = TimeFrameIndicatorView.axisText(forTimeOffset: currentAxisMarkTimeOffset)
            let labelSize = labelText.boundingBox(font: TimeFrameIndicatorView.axisLabelFont)
            
            let textPositionX = markPositionX - labelSize.width * 0.5
            let positionY: CGFloat = axisIndex % 2 == 0 ? 0 : labelSize.height
            
            let labelPosition = CGPoint(x: textPositionX, y: positionY)
            let labelRect = CGRect(origin: labelPosition, size: labelSize)
            (labelText as NSString).draw(in: labelRect, withAttributes: [NSAttributedString.Key.font: TimeFrameIndicatorView.axisLabelFont])
            
            currentAxisMarkTimeOffset += axisMarksOffsetSeconds
            axisIndex += 1
        }
    }
    
    private func drawAxisMark(offsetX: CGFloat) {
        let barFrame = timeScaleBarFrame()
        let lineOffsetY: CGFloat = barFrame.origin.y + barFrame.height * 0.5
        
        let context = UIGraphicsGetCurrentContext()!
        context.setLineWidth(barFrame.height)
        context.setStrokeColor(UIColor.white.cgColor)
        
        context.move(to: CGPoint(x: offsetX - TimeFrameIndicatorView.axisMarkWidth * 0.5, y: lineOffsetY))
        context.addLine(to: CGPoint(x: offsetX + TimeFrameIndicatorView.axisMarkWidth * 0.5, y: lineOffsetY))
        context.strokePath()
    }
    
    private static func axisText(forTimeOffset: TimeInterval) -> String {
        let hours = Int(floor(forTimeOffset / 3600))
        let minutes = Int(forTimeOffset.truncatingRemainder(dividingBy: 3600) / 60)
        return String(format: "%02d:%02d", hours, minutes)
    }
}
