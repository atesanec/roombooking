//
//  MultilineTextFieldCell.swift
//  RoomBooking
//
//  Created by VI_Business on 07/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift
import GrowingTextView

/**
 *  Multiline text field
 */
class MultilineTextFieldCell: UICollectionViewCell, GrowingTextViewDelegate {
    struct Configuration {
        let textFont: UIFont
        let placeholder: String
        let text: String?
        let textChangedObserver: AnyObserver<String?>
        let heightChangedObserver: AnyObserver<CGFloat>
    }
    
    static let minCellHeight: CGFloat = 50
    static let textVerticalInset: CGFloat = 10
    var disposeBag = DisposeBag()
    
    private var heightChangeObserver: AnyObserver<CGFloat>?
    private let textField = GrowingTextView()
    private let separator = OnePixelSeparatorView()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        contentView.addSubview(textField)
        contentView.addSubview(separator)
        
        textField.delegate = self
        textField.minHeight = MultilineTextFieldCell.minCellHeight - MultilineTextFieldCell.textVerticalInset
        textField.textContainerInset = .zero
        textField.textContainer.lineFragmentPadding = 0
        textField.showsVerticalScrollIndicator = false
    }
    
    func configure(info: Configuration) {
        textField.font = info.textFont
        textField.placeholder = info.placeholder
        textField.text = info.text
        textField.rx.text.bind(to: info.textChangedObserver).disposed(by: disposeBag)
        
        heightChangeObserver = info.heightChangedObserver
    }
    
    override func prepareForReuse() {
        disposeBag = DisposeBag()
        heightChangeObserver = nil
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let inset = MultilineTextFieldCell.textVerticalInset
        let textFrame = CGRect(x: 0, y: inset, width: bounds.width, height: bounds.height - 2 * inset)
        textField.frame = textFrame
        
        separator.placeIntoView(position: .bottom)
    }
    
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        heightChangeObserver?.onNext(height)
    }
}
