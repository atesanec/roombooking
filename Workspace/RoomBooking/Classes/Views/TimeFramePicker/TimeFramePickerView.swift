//
//  TimeFramePickerView.swift
//  RoomBooking
//
//  Created by VI_Business on 07/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import RxSwift

/**
 *  Allows selection of time interval
 */
class TimeFramePickerView: UIView, UIGestureRecognizerDelegate {
    typealias SelectedTimeInterval  = (from: TimeInterval, to: TimeInterval)
    
    var minTimeOffsetSeconds: TimeInterval = 0 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    var maxTimeOffsetSeconds: TimeInterval = 60 * 60 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    var intervalFillColor: UIColor = .yellow {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    var intervalBarHeight: CGFloat = 0 {
        didSet {
            setNeedsLayout()
        }
    }
    
    var selectedTimeInterval: SelectedTimeInterval {
        get {
            let timeIntervals = pins.map {$0.timeInterval}
            return (from: timeIntervals.min()!, to: timeIntervals.max()!)
        }
        
        set(interval) {
            pins[0].timeInterval = interval.from
            pins[1].timeInterval = interval.to
            self.setNeedsLayout()
            self.setNeedsDisplay()
        }
    }
    
    let timeIntervalSelectedSignal = PublishSubject<SelectedTimeInterval>()
    
    private let pins = [TimeFramePickerTimePin(), TimeFramePickerTimePin()]
    private var pannedPin: TimeFramePickerTimePin?
    private var panRecognizer: UIPanGestureRecognizer!
    
    private static let minDistanceForPanning: CGFloat = 30
    private static let pinWidth: CGFloat = 50
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        clipsToBounds = false
        contentMode = .redraw
        backgroundColor = .clear
        
        panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(onPanGesture(recognizer:)))
        panRecognizer.delegate = self
        addGestureRecognizer(panRecognizer)
        
        for pin in pins {
            addSubview(pin)
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        let range = selectedTimeInterval
        let offsetY = intervalBarHeight * 0.5
        
        let context = UIGraphicsGetCurrentContext()!
        context.setLineWidth(intervalBarHeight)
        context.move(to: CGPoint(x: timeOffsetToCanvasOffset(timeOffset: range.from), y: offsetY))
        context.setStrokeColor(intervalFillColor.cgColor)
        context.addLine(to: CGPoint(x: timeOffsetToCanvasOffset(timeOffset: range.to), y: offsetY))
        context.strokePath()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        for pin in pins {
            let offsetX = timeOffsetToCanvasOffset(timeOffset: pin.timeInterval)
            let pinSize = CGSize(width: TimeFramePickerView.pinWidth, height: bounds.height)
            pin.frame = CGRect(origin: CGPoint(x: offsetX - pinSize.width * 0.5, y: 0), size: pinSize)
        }
    }
    
    @objc private func onPanGesture(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            pannedPin = findClosestPin(toPoint: recognizer.location(in: self))
        
        case .changed:
            let timeOffset = canvasOffsetToTimeOffset(canvasOffset: recognizer.location(in: self).x)
            pannedPin?.timeInterval = timeOffset
            timeIntervalSelectedSignal.onNext(selectedTimeInterval)
            setNeedsLayout()
            setNeedsDisplay()
            
        case .cancelled, .ended:
            pannedPin = nil
        
        default:
            pannedPin = nil
        }
    }
    
    private func timeOffsetToCanvasOffset(timeOffset: TimeInterval) -> CGFloat {
        assert(timeOffset >= minTimeOffsetSeconds)
        return ceil(CGFloat((timeOffset - minTimeOffsetSeconds) / (maxTimeOffsetSeconds - minTimeOffsetSeconds)) * bounds.width)
    }
    
    private func canvasOffsetToTimeOffset(canvasOffset: CGFloat) -> TimeInterval {
        return TimeInterval(min(bounds.width, max(canvasOffset, 0)) / bounds.width) * (maxTimeOffsetSeconds - minTimeOffsetSeconds)
            + minTimeOffsetSeconds
    }
    
    private func findClosestPin(toPoint: CGPoint) -> TimeFramePickerTimePin? {
        var minDistance = CGFloat.greatestFiniteMagnitude
        var closestPin: TimeFramePickerTimePin? = nil
        for pin in pins {
            // Calculate distance from pin's bounds
            let distance = max(0, abs(pin.center.x - toPoint.x) - pin.bounds.midX)
            if distance < minDistance {
                minDistance = distance
                closestPin = pin
            }
        }
        
        return minDistance <= TimeFramePickerView.minDistanceForPanning ? closestPin : nil
    }
    
    // MARK: Gesture recognizer delegate
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return findClosestPin(toPoint: gestureRecognizer.location(in: self)) != nil
    }
}

fileprivate class TimeFramePickerTimePin: UIView {
    var timeInterval: TimeInterval = 0 {
        didSet {
            updateLabel()
        }
    }
    
    private static let labelFont = UIFont.systemFont(ofSize: 12)
    private static let labelBorderWidth: CGFloat = 1
    private static let labelBorderCornerRadius: CGFloat = 4
    private static let labelBorderColor = UIColor.lightGray
    private static let labelBackgroundColor = UIColor.white
    private static let labelTextVerticalInset: CGFloat = 0
    private static let arrowWidth: CGFloat = 1 / UIScreen.main.scale
    
    private let textLayer = CATextLayer()
    private let arrowLayer = CAShapeLayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        layer.addSublayer(textLayer)
        layer.addSublayer(arrowLayer)
        
        self.isUserInteractionEnabled = false
        self.clipsToBounds = false
        
        textLayer.font = TimeFramePickerTimePin.labelFont
        textLayer.foregroundColor = UIColor.black.cgColor
        textLayer.fontSize = TimeFramePickerTimePin.labelFont.pointSize
        textLayer.borderWidth = TimeFramePickerTimePin.labelBorderWidth
        textLayer.borderColor = TimeFramePickerTimePin.labelBorderColor.cgColor
        textLayer.cornerRadius = TimeFramePickerTimePin.labelBorderCornerRadius
        textLayer.backgroundColor = TimeFramePickerTimePin.labelBackgroundColor.cgColor
        textLayer.alignmentMode = .center
        textLayer.contentsScale = UIScreen.main.scale
        
        arrowLayer.lineWidth = TimeFramePickerTimePin.arrowWidth
        arrowLayer.strokeColor = TimeFramePickerTimePin.labelBorderColor.cgColor
    }
    
    private func updateLabel() {
        textLayer.string = TimeFramePickerTimePin.labelText(forTimeOffset: timeInterval)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let textLayerHeight = TimeFramePickerTimePin.labelFont.lineHeight + TimeFramePickerTimePin.labelTextVerticalInset * 2
        textLayer.bounds = CGRect(origin: .zero, size: CGSize(width: bounds.width, height: textLayerHeight))
        textLayer.position = CGPoint(x: bounds.width * 0.5, y: bounds.height - textLayerHeight * 0.5)
        
        let arrowPath = UIBezierPath(rect: CGRect(origin: .zero, size: CGSize(width: TimeFramePickerTimePin.arrowWidth,
                                                                              height: bounds.height - textLayerHeight)))
        arrowLayer.path = arrowPath.cgPath
        arrowLayer.position = CGPoint(x: (bounds.width - TimeFramePickerTimePin.arrowWidth) * 0.5, y: arrowLayer.bounds.midY)
    }
    
    private static func labelText(forTimeOffset: TimeInterval) -> String {
        let hours = Int(floor(forTimeOffset / 3600))
        let minutes = Int(forTimeOffset.truncatingRemainder(dividingBy: 3600) / 60)
        return String(format: "%02d:%02d", hours, minutes)
    }
}
