//
//  OnePixelSeparatorView.swift
//  RoomBooking
//
//  Created by VI_Business on 08/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

private let onePixelInPoints: CGFloat = 1 / UIScreen.main.scale

/**
 *  One pixel separator
 */
class OnePixelSeparatorView: UIView {
    enum PositionInContainer {
        case top
        case bottom
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        backgroundColor = .lightGray
    }

    public override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: onePixelInPoints)
    }
    
    public override func sizeThatFits(_ size: CGSize) -> CGSize {
        return  CGSize(width: size.width, height: onePixelInPoints)
    }

    func placeIntoView(position: PositionInContainer, offsetX: CGFloat = 0) {
        sizeToFit()
        var offsetY: CGFloat = 0
        switch position {
        case .top:
            offsetY = 0
        case .bottom:
            offsetY = superview!.bounds.height - bounds.height
        }
        
        var separatorFrame = frame
        separatorFrame.origin = CGPoint(x: offsetX, y:offsetY)
        separatorFrame.size.width = superview!.bounds.width - offsetX
        frame = separatorFrame
    }
}
