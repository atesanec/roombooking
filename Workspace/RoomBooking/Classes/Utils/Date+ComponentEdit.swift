//
//  Date+ComponentEdit.swift
//  RoomBooking
//
//  Created by VI_Business on 03/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

extension Date {
    /// Truncate all date components up to day
    func truncatedToDay(fromTimeZone: TimeZone) -> Date {
        var comp: DateComponents = Calendar.current.dateComponents([.year, .month, .day], from: self)
        comp.timeZone = fromTimeZone
        let truncated = Calendar.current.date(from: comp)!
        return truncated
    }
    
    func convert(fromZone: TimeZone, toZone: TimeZone) -> Date {
        return convertToGlobalTime(fromZone: fromZone).convertGlobalTime(toZone: toZone)
    }
    
    func convertToGlobalTime(fromZone: TimeZone) -> Date {
        let seconds = -TimeInterval(fromZone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    func convertGlobalTime(toZone: TimeZone) -> Date {
        let seconds = TimeInterval(toZone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
}
