//
//  AppAppearanceConfigurator.swift
//  RoomBooking
//
//  Created by VI_Business on 08/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

/**
 * Confidurates the global look of the application
 */
class AppAppearanceConfigurator {
    public static func configure() {
        let navBar = UINavigationBar.appearance()
        navBar.barTintColor = .minsk
        navBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        UIBarButtonItem.appearance().tintColor = .white
        
        UISearchBar.appearance().barTintColor = .minsk
    }
}
