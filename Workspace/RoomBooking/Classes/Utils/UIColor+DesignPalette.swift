//
//  UIColor+DesignPalette.swift
//  RoomBooking
//
//  Created by VI_Business on 04/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit
import UIColor_Hex_Swift

extension UIColor {
    static let greenHaze = {
        return UIColor("#009547")
    }()
    
    static let alizarinCrimson = {
        return UIColor("#E31E2F")
    }()
    
    static let turbo = {
        return UIColor("#FFE900")
    }()
    
    static let tango = {
        return UIColor("#EC7B23")
    }()
    
    static let minsk = {
        return UIColor("#362A83")
    }()
    
    static let celurean = {
        return UIColor("#00A0DB")
    }()
}
