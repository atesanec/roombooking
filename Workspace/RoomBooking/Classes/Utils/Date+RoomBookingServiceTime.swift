//
//  Date+RoomBookingServiceTime.swift
//  RoomBooking
//
//  Created by VI_Business on 04/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

extension Date {
    static let roomBookingServiceTimeZone: TimeZone = {
        return TimeZone(identifier: "Europe/Berlin")!
    }()
    
    func localTimeToRoomBookingServiceTime() -> Date {
        return convert(fromZone: TimeZone.current, toZone: Date.roomBookingServiceTimeZone)
    }
    
    func roomBookingServiceTimeToGlobalTime() -> Date {
        return convertToGlobalTime(fromZone: Date.roomBookingServiceTimeZone)
    }
    
    func globalTimeToRoomBookingServiceTime() -> Date {
        return convertGlobalTime(toZone: Date.roomBookingServiceTimeZone)
    }
}
