//
//  AppDelegate.swift
//  RoomBooking
//
//  Created by VI_Business on 03/12/2018.
//  Copyright © 2018 coolcorp. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow()
        window?.rootViewController = UINavigationController(rootViewController: AvailableRoomsListViewController())
        window?.makeKeyAndVisible()
        
        AppAppearanceConfigurator.configure()
        
        return true
    }

}

