# Product Description

App provides user with room booking interface for properties in Berlin. 

It consists of three logical parts:
1. List of rooms available to date
2. Detailed room description
3. Room booking interface

# Tech Description
App architecture consists of several layers:
1. Networking
2. Mediators
3. Screens

**Networking** layer is presented by these main classes implementing general networking tasks
*NetworkPhotoService* implements network request logic and response mapping logic. 
*NetworkImageDownloader* handles image downloading and caching

**Mediators** establish opaque connection between UI and business logic
*CollectionViewSectionManagerAggregate* and *CollectionViewSectionManager* allow decomposing collectionView section logic to loosely coupled entities
*EntityCollectionMonitor* tracks changes of specific entity collection in data storage

**Screens** are generally built using MVVM approach with each screen consisting of viewModel, viewController, section managers and probably action managers
The latter ones are responsible for complex business logic had to be moved outside view controller 

# Future Plans
1. Add unit tests for network requests
2. Implement bookings management logic (resend invites, modify existing booking)
3. Support room booking in various time zones (will require some work on server)
4. Support syncing booking info with server
5. Improve UX for timeframe picker

# Installation notes
Run **pod update** from the command line with current directory set to project root directory (if necessary) 
